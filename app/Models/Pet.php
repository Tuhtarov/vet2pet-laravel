<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Pet
 *
 * @property int $id
 * @property string $name
 * @property float|null $weight
 * @property int|null $chip_number
 * @property int $pet_type_id
 * @property int|null $detention_condition_id
 * @property int|null $food_brand_id
 * @property int $pet_breed_id
 * @property int $gender_id
 * @property int|null $origin_id
 * @property int|null $sterialization_id
 * @property int|null $nutrition_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PetBreed|null $breed
 * @property-read \App\Models\DetentionCondition|null $detention
 * @property-read \App\Models\FoodBrand|null $foodBrand
 * @property-read \App\Models\Gender|null $gender
 * @property-read \App\Models\HealthConcern|null $healthConcerns
 * @property-read \App\Models\Nutrition|null $nutrition
 * @property-read \App\Models\Origin|null $origin
 * @property-read \App\Models\HealthConcern|null $procedures
 * @property-read \App\Models\Sterialization|null $sterialization
 * @property-read \App\Models\PetType|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Pet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pet newQuery()
 * @method static \Illuminate\Database\Query\Builder|Pet onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Pet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereChipNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereDetentionConditionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereFoodBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereNutritionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet wherePetBreedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet wherePetTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereSterializationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|Pet withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Pet withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $place_condition_id
 * @property string|null $birthday
 * @property-read \App\Models\PetPhoto|null $image
 * @property-read \App\Models\PetType $petType
 * @property-read \App\Models\PlaceCondition|null $place
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pet wherePlaceConditionId($value)
 * @property-read int|null $procedures_count
 * @property string|null $health_concerns
 * @method static \Illuminate\Database\Eloquent\Builder|Pet whereHealthConcerns($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PetPhoto[] $photos
 * @property-read int|null $photos_count
 */
class Pet extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'weight',
        'chip_number',
        'birthday',
        'detention_condition_id',
        'place_condition_id',
        'pet_type_id',
        'food_brand_id',
        'pet_breed_id',
        'gender_id',
        'origin_id',
        'sterialization_id',
        'nutrition_id',
        'user_id',
        'health_concerns'
    ];

    public function petType()
    {
        return $this->belongsTo(PetType::class);
    }

    public function origin()
    {
        return $this->belongsTo(Origin::class);
    }

    public function sterialization()
    {
        return $this->belongsTo(Sterialization::class);
    }

    public function foodBrand()
    {
        return $this->belongsTo(FoodBrand::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function detention()
    {
        return $this->belongsTo(DetentionCondition::class, 'detention_condition_id', 'id');
    }

    public function nutrition()
    {
        return $this->belongsTo(Nutrition::class);
    }

    public function breed()
    {
        return $this->belongsTo(PetBreed::class, 'pet_breed_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->hasOne(PetPhoto::class)->where('is_current', true);
    }

    public function photos()
    {
        return $this->hasMany(PetPhoto::class, 'pet_id', 'id');
    }

    public function place()
    {
        return $this->belongsTo(PlaceCondition::class, 'place_condition_id', 'id');
    }

    public function procedures()
    {
        return $this->hasMany(PetProcedure::class, 'pet_id', 'id');
    }
}

