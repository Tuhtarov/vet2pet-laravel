<?php

namespace App\Http\Resources\Vet;

use App\Http\Resources\Client\Consultation\AttachmentsResource;
use App\Http\Resources\Pet\PetResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\VetServiceResource;
use App\Models\VetConsultation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetConsultation
 */
class ConsultationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'symptoms' => $this->symptoms,
            'date_symptoms' => $this->date_symptoms,
            'comment' => $this->comment,
            'commentChat' => $this->commentChat,
            'pet_id' => $this->pet_id,
            'is_done' => $this->is_done,
            'term' => $this->term,
            'vet_id' => $this->vet_id,
            'vet_consultation_type_id' => $this->vet_consultation_type_id,
            'vet_service_id' => $this->vet_service_id,

            'vet_response' => ConsultationResponseResource::make($this->response),

            'vet_full_name' => $this->vet?->getFullName(),
            'vet' => UserResource::make($this->vet),

            'client_id' => $this->client_id,
            'client' => UserResource::make($this->client),

            'pet_type' => $this->pet?->petType?->name,
            'pet_name' => $this->pet?->name,
            'pet' => PetResource::make($this->pet),

            'owner' => UserResource::make($this->pet?->user),
            'owner_full_name' => $this->pet?->user?->getFullName(),

            'service' => VetServiceResource::make($this->service),
            'service_price' => $this->service?->price,

            'attachments' => AttachmentsResource::collection($this->attachments),

            'created' => $this->created_at?->format('Y-m-d H:i')
        ];
    }
}
