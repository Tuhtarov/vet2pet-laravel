<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PetBreed
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed query()
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $pet_type_id
 * @method static \Illuminate\Database\Eloquent\Builder|PetBreed wherePetTypeId($value)
 * @property-read \App\Models\PetType|null $petType
 * @property-read int|null $pets_count
 */
class PetBreed extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'pet_type_id'
    ];

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

    public function petType()
    {
        return $this->belongsTo(PetType::class);
    }
}
