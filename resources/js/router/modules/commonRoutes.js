import {setLayout, setNavLink} from "@/router/metaUtils";
import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";

import vetRoutes from "@/router/modules/vetRoutes";
import adminRoutes from "@/router/modules/adminRoutes";
import clientRoutes from "@/router/modules/clientRoutes";

const role = document.querySelector('meta[name="user-role"]').content

let specificRoutes = [];

if (role === 'vet') {
    specificRoutes = vetRoutes
}

if (role === 'admin') {
    specificRoutes = adminRoutes
}

if (role === 'client') {
    specificRoutes = clientRoutes
}

let baseRoutes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Главная",
                icon: "mdi-home"
            }),
            role: 'common'
        }
    },

    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            ...setLayout('Login')
        }
    },

    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            ...setLayout('Login')
        }
    },

    ...specificRoutes,

    // ошибки
    // {
    //     path: '*',
    //     name: '404',
    //     component: () => import('@/views/errors/E404.vue'),
    //     ...setLayout('Error')
    // }
]

export default baseRoutes
