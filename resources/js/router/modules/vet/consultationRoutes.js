import {setLayout, setNavLink} from "@/router/metaUtils";
import My from "@/views/vet/consultations/My.vue";
import Show from "@/views/vet/consultations/Show.vue";
import Response from "@/views/vet/consultations/Response.vue";

export default [
    {
        path: '/my-consultations',
        name: 'consultations',
        component: My,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Мои консультации",
                icon: "mdi-format-list-bulleted"
            }),
            role: 'vet'
        }
    },

    {
        path: '/my-consultation/:id',
        name: 'my-consultation-show',
        component: Show,
        meta: {
            ...setLayout('App'),
            role: 'vet'
        }
    },
    {
        path: '/my-consultation/:consultationId/response',
        name: 'my-consultation-response',
        component: Response,
        meta: {
            ...setLayout('App'),
            role: 'vet'
        }
    },
    {
        path: '/my-consultation/:consultationId/response/:id',
        name: 'my-consultation-response-update',
        component: Response,
        meta: {
            ...setLayout('App'),
            role: 'vet'
        }
    }
];
