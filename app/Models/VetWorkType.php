<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetWorkType
 *
 * @property int $id
 * @property string $name
 * @property-read \App\Models\VetPersonalInformation|null $vetPersonalInformations
 * @method static \Illuminate\Database\Eloquent\Builder|VetWorkType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetWorkType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetWorkType query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetWorkType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetWorkType whereName($value)
 * @mixin \Eloquent
 */
class VetWorkType extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function vetPersonalInformations()
    {
        return $this->hasOne(VetPersonalInformation::class);
    }
}
