<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetEducation
 *
 * @property int $id
 * @property string $name
 * @property int $vet_personal_information_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\VetEducationFile|null $files
 * @property-read \App\Models\VetPersonalInformation|null $vetPersonalInformation
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducation whereVetPersonalInformationId($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 */
class VetEducation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'vet_personal_information_id',
    ];

    public function vetPersonalInformation()
    {
        return $this->hasOne(VetPersonalInformation::class);
    }

    public function files()
    {
        return $this->hasMany(VetEducationFile::class);
    }
}
