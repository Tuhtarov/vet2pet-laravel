<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class AdminManageRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user()->role->slug === 'admin';
    }

    public function rules()
    {
        $VET_ROLE_ID = 2;
        $requestRule = '';
        $additionalRules = [];
        $is = request()->request->get('role_id');

        if (request()->routeIs('user.admin-store')) {
            $additionalRules = [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];

            $requestRule = 'required';

        } else if (request()->routeIs('user.admin-update')) {
            $requestRule = 'sometimes';

            if ($is === $VET_ROLE_ID) {
                $additionalRules = array_merge($additionalRules, [
                    'vet_personal_information_id' => [$requestRule, 'integer'],
                    'bank_id' => [$requestRule, 'integer'],
                ]);
            }
        }

        if ($is === $VET_ROLE_ID) {
            $additionalRules = array_merge($additionalRules, [
                'vet_work_type_id' => [$requestRule, 'integer'],
                'address' => [$requestRule, 'string'],
                'passport_series' => [$requestRule, 'integer'],
                'passport_number' => [$requestRule, 'integer'],
                'education_name' => [$requestRule, 'string'],

                'settlement_account' => [$requestRule, 'integer'],
                'bank_name' => [$requestRule, 'string'],
                'bank_address' => [$requestRule, 'string'],
                'bank_correspondent_account' => [$requestRule, 'integer'],
                'bik' => [$requestRule, 'integer'],
            ]);
        }

        return array_merge($additionalRules, [
            'name' => [$requestRule, 'string', 'max:255', 'min:2'],
            'surname' => [$requestRule, 'string', 'min:2'],
            'patronymic' => ['sometimes', 'nullable', 'string', 'min:2'],
            'phone' => [$requestRule, 'integer', 'min:9'],
            'email' => [$requestRule, 'string', 'email', 'max:255', 'unique:users'],
            'birthday' => [$requestRule, 'date'],
            'city' => ['string', 'min:2'],
            'role_id' => [$requestRule, 'integer']
        ]);
    }
}
