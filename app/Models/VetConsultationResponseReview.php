<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationResponseReview
 *
 * @property int $id
 * @property int $stars
 * @property string $comment
 * @property int $client_id
 * @property int $vet_consultation_response_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereStars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponseReview whereVetConsultationResponseId($value)
 * @mixin \Eloquent
 */
class VetConsultationResponseReview extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'vet_consultation_response_id',
        'stars',
        'comment',
    ];
}
