import './bootstrap'
import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/src/css/index.sass'

import vuetify from "@/plugins/vuetify";
import { Quasar } from 'quasar'
import vuex from '@/store';
import router from "@/router";

import App from '@/App.vue';
import { createApp } from 'vue';
const app = createApp({});

app.use(vuex)
app.use(vuetify)
app.use(router)

app.use(Quasar, {
    plugins: {}
})

app.component('App', App)

app.mount('#app');
