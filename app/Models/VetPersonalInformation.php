<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetPersonalInformation
 *
 * @property int $id
 * @property string $address
 * @property int $passport_series
 * @property int $passport_number
 * @property int $is_activity
 * @property int $vet_work_type_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\VetBankDetail|null $bankDetail
 * @property-read \App\Models\VetEducation|null $educations
 * @property-read \App\Models\VetPassportFile|null $passportFiles
 * @property-read \App\Models\VetWorkType|null $workType
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereIsActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation wherePassportSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPersonalInformation whereVetWorkTypeId($value)
 * @mixin \Eloquent
 * @property-read int|null $educations_count
 */
class VetPersonalInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'vet_work_type_id',

        'address',
        'passport_series',
        'passport_number',
        'user_id',
        'is_activity'
    ];

    public function workType()
    {
        return $this->belongsTo(VetWorkType::class);
    }

    public function bankDetail()
    {
        return $this->hasOne(VetBankDetail::class, 'vet_personal_information_id', 'id');
    }

    public function passportFiles()
    {
        return $this->belongsTo(VetPassportFile::class);
    }

    public function educations()
    {
        return $this->hasMany(
            VetEducation::class,
            'vet_personal_information_id',
            'id'
        );
    }
}
