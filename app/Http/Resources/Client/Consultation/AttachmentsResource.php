<?php

namespace App\Http\Resources\Client\Consultation;

use App\Models\VetConsultationAttachment;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetConsultationAttachment
 */
class AttachmentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'attachment_id' => $this->id,

            'file_id' => $this->file_id,
            'name' => $this->file->name,
            'path' => $this->file->path
        ];
    }
}
