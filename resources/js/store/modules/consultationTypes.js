import axios from '@/api/Axios';

export default {
    state: {
        consultationTypes: [],
    },
    getters: {
        all: state => state.consultationTypes,
    },
    mutations: {
        setAll: (state, data) => {
            state.consultationTypes = data
        },

        add: (state, data) => {
            state.consultationTypes.push(data);
        },
    },
    actions: {
        async fetchAll({commit}) {
            return await axios.get("/consultation-type").then(({data}) => {
                commit('setAll', data)
                return data
            })
        },

        async create({commit}, entity) {
            return await axios.post("/consultation-type", entity).then(res => {
                commit('add', res.data)
                return res
            })
        },
    },
    namespaced: true,
}
