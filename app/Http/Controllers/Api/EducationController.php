<?php

namespace App\Http\Controllers\Api;

use App\Contracts\IFileUploader;
use App\Http\Controllers\Controller;
use App\Http\Resources\EducationResource;
use App\Models\VetEducation;
use App\Models\VetEducationFile;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function __construct(private IFileUploader $fileUploader)
    {
    }

    public function addEducationImages(Request $request, $id)
    {
        $vetEducation = VetEducation::findOrFail($id);

        $files = $request->allFiles();

        foreach ($files as $file) {
            $savedFile = $this->fileUploader->handle($file);

            $educationFile = new VetEducationFile();
            $educationFile->file_id = $savedFile->id;
            $educationFile->vet_education_id = $vetEducation->id;
            $educationFile->save();
        }

        return EducationResource::make(
            VetEducation::find($vetEducation->id)
        );
    }

    public function show($id)
    {
        return EducationResource::make(
            VetEducation::find($id)
        );
    }

    public function deleteEducationFile($id)
    {
        $educationFile = VetEducationFile::find($id);
        $educationFile->delete();

        return response()->json([
            'result' => $educationFile->trashed() ? 'deleted' : 'not deleted'
        ]);
    }
}
