import baseStore from '@/store/modules/pets/baseStore'

export default {
    ...baseStore('/pet-breed'),
    namespaced: true,
};
