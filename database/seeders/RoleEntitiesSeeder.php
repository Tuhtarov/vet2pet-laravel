<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleEntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Администратор' => 'admin', 'Ветеринар' => 'vet', 'Клиент' => 'client'] as $val => $key) {
            $entity = new Role();
            $entity->name = $val;
            $entity->slug = $key;
            $entity->save();
        }
    }
}
