<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FoodBrand
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand query()
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FoodBrand whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FoodBrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }
}
