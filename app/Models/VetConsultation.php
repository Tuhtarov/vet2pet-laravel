<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\VetConsultation
 *
 * @property int $id
 * @property string $name
 * @property string $symptoms
 * @property string|null $comment
 * @property string $date_symptoms
 * @property int $is_done
 * @property int|null $vet_consultation_type_id
 * @property int $vet_service_id
 * @property int|null $vet_id
 * @property int $pet_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereDateSymptoms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereIsDone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation wherePetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereSymptoms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereVetConsultationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereVetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereVetServiceId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\VetConsultationType|null $consultationType
 * @property-read \App\Models\Pet $pet
 * @property-read \App\Models\VetService|null $service
 * @property-read \App\Models\User|null $vet
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $pet_owner_id
 * @property-read \App\Models\User|null $petOwner
 * @method static \Illuminate\Database\Query\Builder|VetConsultation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation wherePetOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|VetConsultation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|VetConsultation withoutTrashed()
 * @property int|null $anonymous_pet_owner_id
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereAnonymousPetOwnerId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VetConsultationAttachment[] $attachments
 * @property-read int|null $attachments_count
 * @property int|null $client_id
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultation whereClientId($value)
 */
class VetConsultation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'symptoms',
        'comment',
        'commentChat',
        'date_symptoms',
        'vet_consultation_type_id',
        'vet_service_id',
        'vet_id',
        'pet_id',
        'client_id',
        'term'
    ];

    protected $casts = [
        'pet_id' => 'integer',
        'vet_id' => 'integer',
        'vet_consultation_type_id' => 'integer',
        'vet_service_id' => 'integer',
        'client_id' => 'integer',
    ];

    public function vet()
    {
        return $this->belongsTo(User::class, 'vet_id', 'id');
    }

    public function response()
    {
        return $this->hasOne(VetConsultationResponse::class, 'vet_consultation_id', 'id');
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id', 'id');
    }

    public function petOwner()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(VetConsultationAttachment::class, 'vet_consultation_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'vet_consultation_id', 'id')->with(['user', 'attachments', 'attachments.file']);
    }

    public function service()
    {
        return $this->belongsTo(VetService::class, 'vet_service_id', 'id');
    }

    public function consultationType()
    {
        return $this->belongsTo(VetConsultationType::class, 'vet_consultation_type_id', 'id');
    }
}
