<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pet_breeds', function (Blueprint $table) {
            $table->foreignId('pet_type_id')->nullable();
            $table->foreign('pet_type_id')
                ->on('pet_types')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_breeds', function (Blueprint $table) {
            $table->dropForeign('pet_breeds_pet_type_id_foreign');
            $table->dropColumn('pet_type_id');
        });
    }
};
