<?php

namespace App\Http\Resources;

use App\Models\VetEducationFile;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetEducationFile
 */
class EducationFilesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file_id' => $this->file_id,
            'name' => $this->file->name,
            'path' => $this->file->path
        ];
    }
}
