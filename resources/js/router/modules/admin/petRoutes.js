import {setLayout, setNavLink} from "@/router/metaUtils";
import All from "@/views/admin/pets/Pets.vue";
import Show from "@/views/client/pets/Show.vue";
import Form from "@/views/client/pets/Form.vue";

export default [
    {
        path: '/pets',
        name: 'pets',
        component: All,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Питомцы",
                icon: "mdi-dog-side"
            }),
            role: 'admin'
        }
    },

    {
        path: '/pet/:id',
        name: 'pet-show',
        component: Show,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/pet-edit/:id',
        name: 'pet-edit',
        component: Form,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
]
