<?php

namespace App\Contracts;

use App\Models\Role;
use Illuminate\Support\Facades\Hash;

abstract class IUserRegister
{
    public function register(array $data, string $roleSlug = 'vet'): array
    {
        $role = Role::whereSlug($roleSlug)->first();

        $data['role_id'] = $role->id;
        $data['password'] = Hash::make($data['password']);

        return $data;
    }
}
