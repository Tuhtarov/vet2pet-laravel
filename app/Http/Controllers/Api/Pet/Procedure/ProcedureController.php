<?php

namespace App\Http\Controllers\Api\Pet\Procedure;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\ProcedureResource;
use App\Models\PetProcedure;
use Illuminate\Http\Request;

class ProcedureController extends Controller
{
    public function index()
    {
        return ProcedureResource::collection(
            PetProcedure::all()
        );
    }

    public function delete(PetProcedure $procedure)
    {
        return response()->json([
            'result' => $procedure->deleteOrFail()
        ]);
    }
}
