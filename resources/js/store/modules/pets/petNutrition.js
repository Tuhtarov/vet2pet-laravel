import baseStore from '@/store/modules/pets/baseStore'

export default {
    ...baseStore('/nutrition'),
    namespaced: true,
};
