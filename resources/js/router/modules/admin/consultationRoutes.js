import ConsultAll from "@/views/admin/consultations/All.vue";
import ConsultForm from "@/views/admin/consultations/Form.vue";
import ConsultShow from "@/views/admin/consultations/Show.vue";
import Response from "@/views/admin/consultations/Response.vue";

import {setLayout, setNavLink} from "@/router/metaUtils";

export default [
    {
        path: '/all-consultations',
        name: 'all-consultations',
        component: ConsultAll,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Заявки",
                icon: "mdi-format-list-bulleted"
            }),
            role: 'admin'
        }
    },
    {
        path: '/my-consultation/:consultationId/response',
        name: 'my-consultation-response',
        component: Response,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/my-consultation/:consultationId/response/:id',
        name: 'my-consultation-response-update',
        component: Response,
        meta: {
            ...setLayout('App'),
            role: 'vet'
        }
    },
    {
        path: '/consultation-add',
        name: 'consultation-add',
        component: ConsultForm,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/consultation-edit/:id',
        name: 'consultation-edit',
        component: ConsultForm,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/consultation/:id',
        name: 'consultation-show',
        component: ConsultShow,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    }
]
