<?php

namespace App\Http\Resources\Vet;

use App\Models\VetConsultationType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetConsultationType
 */
class ConsultationTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
