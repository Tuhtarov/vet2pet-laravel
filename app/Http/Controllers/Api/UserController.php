<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function vets()
    {
        $vetRole = Role::whereSlug('vet')->first();

        return UserResource::collection(
            User::whereRoleId($vetRole->id)->get()
        );
    }

    public function update(Request $request, User $user = null)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'surname' => ['required', 'string', 'min:2'],
            'patronymic' => ['sometimes', 'nullable', 'string', 'min:2'],
            'phone' => ['required', 'integer', 'min:9'],
            'birthday' => ['required', 'date'],
            'city' => ['string', 'min:2']
        ]);

        $authUser = $request->user();
        $isAdmin = $authUser->role->slug === 'admin';

        return response()->json(
            $this->updateUser(
                $user && $isAdmin ? $user : $authUser, $data
            )
        );
    }

    public function updateToken(Request $request, User $user)
    {
        $user->token = $request->token;
        $user->save();

        return response()->json($user);
    }

    private function updateUser(User $user, array $data): bool
    {
        return $user->update($data);
    }
}
