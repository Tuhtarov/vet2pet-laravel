<?php

namespace App\Http\Controllers\Api\Vet;

use App\Http\Controllers\Controller;
use App\Http\Resources\VetServiceResource;
use App\Models\VetService;
use Illuminate\Http\Request;

class VetServiceController extends Controller
{
    public function index()
    {
        return VetServiceResource::collection(VetService::all());
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
        ]);

        return VetServiceResource::make(
            VetService::create($data)
        );
    }

    public function show(VetService $service)
    {
        return VetServiceResource::make($service);
    }

    public function update(Request $request, VetService $service)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
        ]);

        $service->update($data);

        return VetServiceResource::make($service);
    }
}
