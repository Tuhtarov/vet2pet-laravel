<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationAttachment
 *
 * @property int $id
 * @property int $vet_consultation_id
 * @property int $file_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationAttachment whereVetConsultationId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\File $file
 */
class VetConsultationAttachment extends Model
{
    use HasFactory;

    protected $fillable = [
        'vet_consultation_id',
        'file_id',
    ];

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }
}
