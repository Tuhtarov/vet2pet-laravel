<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Origin
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|Origin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Origin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Origin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Origin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Origin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Origin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Origin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Origin extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }
}
