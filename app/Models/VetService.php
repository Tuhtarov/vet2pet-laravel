<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetService
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetService query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetService wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VetService extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
    ];
}
