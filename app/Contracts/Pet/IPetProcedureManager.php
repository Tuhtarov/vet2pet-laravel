<?php

namespace App\Contracts\Pet;

use App\Http\Requests\Api\Pet\ManagePetRequest;
use App\Models\Pet;
use App\Models\PetProcedure;

interface IPetProcedureManager
{
    /**
     * Получает массив из новых или существующих сущностей PetProcedure,
     * в зависимости от наличия ключа ID в элементах массива procedures из Request
     * @param ManagePetRequest $request
     * @return array<PetProcedure>
     */
    public function getProceduresFrom(ManagePetRequest $request): array;

    /**
     * @param Pet $pet
     * @param array<PetProcedure> $procedures
     * @return Pet
     */
    public function saveProceduresFor(Pet $pet, array $procedures): Pet;

    /**
     * @param Pet $pet
     * @param ManagePetRequest $request
     * @return Pet
     */
    public function updateProceduresFor(Pet $pet, ManagePetRequest $request): Pet;
}
