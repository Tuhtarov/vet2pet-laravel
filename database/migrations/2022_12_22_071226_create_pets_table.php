<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->double('weight')->nullable();
            $table->unsignedInteger('chip_number')->nullable();

            /* вид животного */
            $table->foreignId('pet_type_id');
            $table->foreign('pet_type_id')->on('pet_types')->references('id');

            /* условия хранения */
            $table->foreignId('detention_condition_id')->nullable();
            $table->foreign('detention_condition_id')
                ->on('detention_conditions')->references('id');

            /* марка корма */
            $table->foreignId('food_brand_id')->nullable();
            $table->foreign('food_brand_id')->on('food_brands')->references('id');

            /* порода животного */
            $table->foreignId('pet_breed_id');
            $table->foreign('pet_breed_id')->on('pet_breeds')->references('id');

            /* пол животного */
            $table->foreignId('gender_id');
            $table->foreign('gender_id')->on('genders')->references('id');

            /* происхождение животного */
            $table->foreignId('origin_id')->nullable();
            $table->foreign('origin_id')->on('origins')->references('id');

            /* стериализация */
            $table->foreignId('sterialization_id')->nullable();
            $table->foreign('sterialization_id')
                ->on('sterializations')->references('id');

            /* стериализация */
            $table->foreignId('nutrition_id')->nullable();
            $table->foreign('nutrition_id')->on('nutrition')->references('id');

            /* владелец (пользователь) */
            $table->foreignId('user_id');
            $table->foreign('user_id')->on('users')->references('id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
};
