<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationHistoryType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistoryType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VetConsultationHistoryType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];
}
