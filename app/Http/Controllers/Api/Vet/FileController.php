<?php

namespace App\Http\Controllers\Api\Vet;

use App\Http\Controllers\Controller;
use App\Models\VetConsultationAttachment;

class FileController extends Controller
{
    public function delete(VetConsultationAttachment $attachment)
    {
        $isDelete = $attachment->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Файл был успешно удален',
            'success' => $isDelete
        ]);
    }
}
