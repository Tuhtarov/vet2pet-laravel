<?php

namespace App\Notifications;

use App\Broadcasting\ChatChannel;
use Illuminate\Notifications\Notification;
use NotificationChannels\PusherPushNotifications\PusherChannel;
use NotificationChannels\PusherPushNotifications\PusherMessage;

class MessageChat extends Notification
{
    public function via($notifiable)
    {
        return [PusherChannel::class, ChatChannel::class];
    }

    public function toPushNotification($notifiable)
    {
        return PusherMessage::create()
            ->android()
            ->title('message')
            ->icon('icon')
            ->body('Вы получили новое сообщение')
            ->setOption('chat','chat');
    }
}
