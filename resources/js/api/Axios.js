import Axios from 'axios';
const BASE_URL = '/api';

let responseCatcher = response => {
    return response
}

let baseConfig = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
    withCredentials: true,
    'X-Requested-With': 'XMLHttpRequest'
}

export let axios = Axios.create({
    baseURL: BASE_URL,
    ...baseConfig,
});

axios.interceptors.response.use(responseCatcher, function (error) {
    return Promise.reject(error);
});

export let axiosLogin = Axios.create(baseConfig);

axiosLogin.interceptors.response.use(responseCatcher, function (error) {
    return Promise.reject(error);
});

export default axios;

export const axiosForUploading = Axios.create({
    baseURL: BASE_URL,
    headers: {
        'Accept': 'application/json'
    },
    withCredentials: true,
    'X-Requested-With': 'XMLHttpRequest'
})
