<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_passport_files', function (Blueprint $table) {
            $table->id();

            $table->foreignId('vet_personal_information_id');
            $table->foreign('vet_personal_information_id')
                ->on('vet_personal_information')->references('id');

            $table->foreignId('file_id');
            $table->foreign('file_id')
                ->on('files')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vet_passport_files');
    }
};
