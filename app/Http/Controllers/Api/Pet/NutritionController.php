<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\NutritionResource;
use App\Models\Nutrition;
use Illuminate\Http\Request;

class NutritionController extends Controller
{
    public function index()
    {
        return NutritionResource::collection(
            Nutrition::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return NutritionResource::make(
            Nutrition::create($data)
        );
    }

    public function show(Nutrition $entity)
    {
        return NutritionResource::make($entity);
    }

    public function update(Request $request, Nutrition $entity)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $entity->update($data);

        return NutritionResource::make($entity);
    }
}
