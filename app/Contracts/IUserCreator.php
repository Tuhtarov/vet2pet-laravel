<?php

namespace App\Contracts;

use App\Models\User;

interface IUserCreator
{
    public function create(array $data): User;
}
