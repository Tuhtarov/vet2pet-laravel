<?php

namespace App\Http\Controllers\Api\Client\Consultation;

use App\Contracts\IFileUploader;
use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Models\ChatMedia;
use App\Models\Message;
use App\Http\Requests\Api\Client\Consultation\{CreateRequest, UpdateRequest};
use App\Http\Resources\Client\Consultation\ConsultationResource;
use App\Models\Pet;
use App\Models\VetConsultation;
use App\Models\VetConsultationAttachment;
use http\Client\Curl\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging\RawMessageFromArray;
use Orhanerday\OpenAi\OpenAi;
use OpenAI\Exceptions\UnserializableResponse;
use Pusher\PushNotifications\PushNotifications;

class ConsultationController extends Controller
{
    public function __construct(private readonly IFileUploader $uploader)
    {
    }

    public function index(Pet $pet = null)
    {
        $query = VetConsultation::whereClientId(request()->user()->id);

        if (!empty($pet)) $query->wherePetId($pet->id);

        return ConsultationResource::collection($query->get());
    }

    public function show(VetConsultation $consultation)
    {
        return ConsultationResource::make(
            VetConsultation::wherePetId($this->getClientPetIds())
                ->orWhere('client_id', request()->user()->id)
                ->whereKey($consultation->id)
                ->firstOrFail()
        );
    }

    public function create(CreateRequest $request)
    {
        $consultationData = $request->validated();

        $petId = $request->input('pet_id');
        $petBelongsToCurrentUser = !empty($petId) && in_array((int)$petId, $this->getClientPetIds());
        if ($petBelongsToCurrentUser || is_null($petId))
            $consultation = new VetConsultation($consultationData);

        if (isset($consultation)) {
            $consultation->client_id = request()->user()->id;
            try {
                $open_ai_key = getenv('OPENAI_API_KEY');
                $open_ai = new OpenAi($open_ai_key);
                $chat = $open_ai->chat([
                    'model' => 'gpt-3.5-turbo',
                    'messages' => [
                        [
                            "role" => "system",
                            "content" => "Какие заболевания можно определить по данным симптомам у животного?"
                        ],
                    ],
                    'temperature' => 1.0,
                    'max_tokens' => 4000,
                    'frequency_penalty' => 0,
                    'presence_penalty' => 0,
                ]);
                $answer = json_decode($chat);
                $consultation->comment = $answer->choices[0]->message->content;
            } catch (UnserializableResponse $exception) {
                 Log::error($exception->getMessage());
            }
            $consultation->saveOrFail();

            $this->saveFiles($request, $consultation);
            return ConsultationResource::make($consultation);
        }

        throw new \InvalidArgumentException(
            "Для консультации был выбран питомец (ID: $petId), принадлежащий другому пользователю."
        );
    }

    function getAnswer($prompts, $inputs, $question)
    {
        // Loops through all the inputs and compare on a cosine similarity to the question and output the correct answer
        $results = [];
        for ($i = 0; $i < count($inputs->embeddings); $i++) {
            $similarity = $this->cosineSimilarity($inputs->embeddings[$i]->embedding, $question->embeddings[0]->embedding);
            // Store the similarity and index in an array and sort by the similarity
            $results[] = [
                'similarity' => $similarity,
                'index' => $i,
                'input' => $prompts[$i],
            ];
        }
        usort($results, function ($a, $b) {
            return $a['similarity'] <=> $b['similarity'];
        });

        return end($results);
    }

    private function cosineSimilarity($u, $v)
    {
        $dotProduct = 0;
        $uLength = 0;
        $vLength = 0;
        for ($i = 0; $i < count($u); $i++) {
            $dotProduct += $u[$i] * $v[$i];
            $uLength += $u[$i] * $u[$i];
            $vLength += $v[$i] * $v[$i];
        }
        $uLength = sqrt($uLength);
        $vLength = sqrt($vLength);
        return $dotProduct / ($uLength * $vLength);
    }

    public function update(VetConsultation $consultation, UpdateRequest $request)
    {
        if (empty($consultation->pet_id) || empty($request->validated()['pet_id']))
            $consultation->client_id = request()->user()->id;

        if ($consultation->updateOrFail($request->validated()))
            $this->saveFiles($request, $consultation);

        return ConsultationResource::make(
            VetConsultation::find($consultation->id)
        );
    }

    public function getMessages(Request $request, VetConsultation $consultation)
    {
        return $consultation->messages;
    }

    public function send(Request $request, VetConsultation $consultation = null)
    {
        $request->validate([
            'message' => 'required|string'
        ]);

        $message = new Message();
        $message->message = $request->message;
        $message->user_id = $request->user()->id;
        $message->vet_consultation_id = $consultation->id;
        $message->save();

        foreach ($request->files->all('attachments') as $file) {
            $chatMedia = new ChatMedia();
            $chatMedia->file_id = $this->uploader->handle($file)->id;
            $chatMedia->message_id =  $message->id;
            $chatMedia->save();
        }

        $client = \App\Models\User::whereId($consultation->client_id)->first();

        $data = [
            'consultationId' => $consultation->id
        ];

        if ($client !== $request->user()) {
            $this->sendFCM(
                "Сообщение от ветеринара в заявке № $consultation->id",
                $message->message,
                $client->token,
                $data
            );
        }

        broadcast(new MessageSent($request->user(), $message, $consultation));

        return $message;
    }

    public function sendF(Request $request)
    {
        $this->sendFCM(
            "",
            "",
            $request->user()->token
            []
        );

        return $request->message;
    }

    private function sendFCM(string $title, string $body, string $token, array $data)
    {

        $SERVER_API_KEY = env('FCM_SERVER_KEY');


        $rawData = [
            "to" => $token,
            "notification" => [
                "title" => $title,
                "body" => $body,
            ],
            "data" => $data
        ];

        $dataString = json_encode($rawData);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
    }

    public function delete(VetConsultation $consultation)
    {
        /**
         * @var VetConsultation $consultation
         */
        $consultation = VetConsultation
            ::wherePetId($this->getClientPetIds())
            ->whereKey($consultation->id)
            ->firstOrFail();

        return response()->json([
            'result' => $consultation->deleteOrFail()
        ]);
    }

    /**
     * @return array<int>
     */
    private function getClientPetIds(): array
    {
        return array_map(
            fn(Pet $pet): int => (int)$pet->id,
            Pet::whereUserId(request()->user()->id)->get()->all()
        );
    }

    /**
     * сохраняем файлы если есть
     * @param CreateRequest $request
     * @param VetConsultation $consultation
     * @return void
     */
    private function saveFiles(FormRequest $request, VetConsultation $consultation): void
    {
        $files = $request->files->all('attachments');

        if (!empty($files)) {
            foreach ($files as $file) {
                VetConsultationAttachment::create([
                    'vet_consultation_id' => $consultation->id,
                    'file_id' => $this->uploader->handle($file)->id
                ]);
            }
        }
    }
}
