<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Sterialization
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sterialization whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sterialization extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }
}
