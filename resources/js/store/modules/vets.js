import axios from '@/api/Axios';

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        async updateEducation({commit}, fields) {
            return await axios.post(`/education/${fields.id}`, fields)
        },
    },
    namespaced: true,
}
