<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\PlaceConditionResource;
use App\Models\PlaceCondition;
use Illuminate\Http\Request;

class PlaceConditionController extends Controller
{
    public function index()
    {
        return PlaceConditionResource::collection(
            PlaceCondition::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return PlaceConditionResource::make(
            PlaceCondition::create($data)
        );
    }

    public function show(PlaceCondition $detention)
    {
        return PlaceConditionResource::make($detention);
    }

    public function update(Request $request, PlaceCondition $detention)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $detention->update($data);

        return PlaceConditionResource::make($detention);
    }
}
