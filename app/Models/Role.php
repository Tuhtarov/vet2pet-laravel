<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User|null $users
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereSlug($value)
 */
class Role extends Model
{
    use HasFactory;
    public $timestamps = false;

    public const CLIENT_SLUG = 'client';

    public function users()
    {
        return $this->hasOne(User::class);
    }
}
