<?php

namespace App\Http\Resources\Pet;

use App\Models\PetPhoto;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PetPhoto
 */
class PetPhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'pet_photo_id' => $this->id,
            'file_id' => $this->file_id,
            'name' => $this->file?->name,
            'path' => $this->file?->path,
            'is_current' => $this->is_current
        ];
    }
}
