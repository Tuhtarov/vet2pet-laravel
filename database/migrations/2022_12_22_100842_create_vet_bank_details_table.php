<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_bank_details', function (Blueprint $table) {
            $table->id();
            $table->string('bank_name');
            $table->text('bank_address');
            $table->unsignedBigInteger('bank_correspondent_account');
            $table->unsignedBigInteger('settlement_account');
            $table->unsignedInteger('bik');

            $table->foreignId('vet_personal_information_id');
            $table->foreign('vet_personal_information_id')
                ->on('vet_personal_information')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vet_bank_details');
    }
};
