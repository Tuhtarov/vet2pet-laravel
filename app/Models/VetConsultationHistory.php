<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationHistory
 *
 * @property int $id
 * @property int $vet_consultation_id
 * @property int $vet_consultation_history_type_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereVetConsultationHistoryTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationHistory whereVetConsultationId($value)
 * @mixin \Eloquent
 */
class VetConsultationHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'vet_consultation_id',
        'vet_consultation_history_id',
        'user_id',
    ];
}
