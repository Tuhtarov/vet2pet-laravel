<?php

namespace App\Http\Controllers\Api\Vet;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Consultation\ConsultationResponseRequest;
use App\Http\Resources\Vet\ConsultationResponseResource;
use App\Models\VetConsultationResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ConsultationResponseController extends Controller
{
    public function index()
    {
        return ConsultationResponseResource::collection(
            VetConsultationResponse::all()
        );
    }

    public function store(ConsultationResponseRequest $request)
    {
        $entity = new VetConsultationResponse($request->all());

        $entity->saveOrFail();

        return ConsultationResponseResource::make($entity);
    }

    public function show(VetConsultationResponse $consultationResponse)
    {
        return ConsultationResponseResource::make($consultationResponse);
    }

    public function update(ConsultationResponseRequest $request, VetConsultationResponse $consultationResponse)
    {
        $consultationResponse->updateOrFail($request->validated());

        return ConsultationResponseResource::make($consultationResponse);
    }

    public function destroy(VetConsultationResponse $consultationResponse)
    {
        $consultationResponse->delete();

        return ConsultationResponseResource::make($consultationResponse);
    }

    public function changeStatus(Request $request, VetConsultationResponse $consultationResponse)
    {
        if ($request->has('reason_deviation')) {
            $consultationResponse->reason_deviation = $request->get('reason_deviation');
        }

        $consultationResponse->status = (int) $request->get('status');
        $consultationResponse->save();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

}
