import baseStore from '@/store/modules/pets/baseStore'

export default {
    ...baseStore('/place-condition'),
    namespaced: true,
};
