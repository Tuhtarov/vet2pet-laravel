import axios from '@/api/Axios';

export default {
    state: {
        genders: [],
    },
    getters: {
        all: state => state.genders,
    },
    mutations: {
        setAll: (state, genders) => {
            state.genders = genders
        },
    },
    actions: {
        async fetchAll({commit}) {
            return await axios.get("/gender")
                .then(({data}) => {
                    commit('setAll', data)
                    return data
                })
        },
    },
    namespaced: true,
}
