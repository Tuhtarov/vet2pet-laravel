import axios from "@/api/Axios";
import {createRouter, createWebHistory} from 'vue-router'
import routes from "@/router/modules/commonRoutes";

const router = createRouter({
    history: createWebHistory(),
    routes
})

async function checkAuth(to, from, next) {
    if (to.name !== 'login' && to.name !== 'register') {
        const status = await axios.get('/user').then((response) => {
            if (to.meta?.role !== 'common' && to.meta?.role !== response.data.user.role_slug) {
                console.error(to)
                next({name: 'home'});
            }

            return response.status;
        }).catch(response => {
            console.error(response)
            return response.status
        })

        if (status !== 200 && status !== 500) {
            next({name: 'login'});
        }
    }

    next()
}

router.beforeEach(checkAuth);

export default router
