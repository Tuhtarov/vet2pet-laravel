<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\DetentionConditionResource;
use App\Models\DetentionCondition;
use Illuminate\Http\Request;

class DetentionConditionController extends Controller
{
    public function index()
    {
        return DetentionConditionResource::collection(
            DetentionCondition::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return DetentionConditionResource::make(
            DetentionCondition::create($data)
        );
    }

    public function show(DetentionCondition $detention)
    {
        return DetentionConditionResource::make($detention);
    }

    public function update(Request $request, DetentionCondition $detention)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $detention->update($data);

        return DetentionConditionResource::make($detention);
    }
}
