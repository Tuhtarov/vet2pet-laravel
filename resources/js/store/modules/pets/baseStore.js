import axios from "@/api/Axios";

export default function getBaseStore(allUri, createUri = '') {
    if (createUri.length < 1) {
        createUri = allUri
    }

    return {
        state: {
            entities: [],
        },

        getters: {
            allEntities: state => state.entities,
        },

        mutations: {
            setEntities: (state, data) => {
                state.entities = data
            },

            addEntity: (state, petType) => {
                state.entities.push(petType);
            },
        },
        actions: {
            async fetchAllEntities({commit, getters}) {
                return await axios.get(allUri)
                    .then(({data}) => {
                        commit('setEntities', data)
                        return getters.allEntities;
                    })
            },

            async create({commit, getters}, data) {
                return await axios.post(createUri, data)
                    .then(({data}) => {
                        console.log(data);
                        commit('addEntity', data)
                        return data
                    })
            },
        },
    };
}
