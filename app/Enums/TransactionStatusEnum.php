<?php

namespace App\Enums;

class TransactionStatusEnum
{
    public const CREATE = 'CREATED';
    public const FAILED = 'FAILED';
    public const CONFIRMED = 'CONFIRMED';
}
