<?php

namespace App\Contracts\Pet;

use App\Http\Requests\Api\Pet\ManagePetRequest;
use App\Models\HealthConcern;
use App\Models\Pet;
use App\Models\PetProcedure;

interface IPetHealthManager
{
    public function set(Pet $pet, ManagePetRequest $request): self;

    public function save(): Pet;

    public function update(): Pet;
}
