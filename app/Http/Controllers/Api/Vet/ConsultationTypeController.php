<?php

namespace App\Http\Controllers\Api\Vet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Vet\ConsultationTypeResource;
use App\Models\VetConsultationType;

class ConsultationTypeController extends Controller
{
    public function index()
    {
        return ConsultationTypeResource::collection(
            VetConsultationType::all()
        );
    }
}
