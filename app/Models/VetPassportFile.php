<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetPassportFile
 *
 * @property int $id
 * @property int $vet_personal_information_id
 * @property int $file_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\VetPersonalInformation|null $vetPersonalInformation
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPassportFile whereVetPersonalInformationId($value)
 * @mixin \Eloquent
 */
class VetPassportFile extends Model
{
    use HasFactory;

    protected $fillable = [
        'file_id',
        'vet_personal_information_id',
    ];

    public function vetPersonalInformation()
    {
        return $this->hasOne(VetPersonalInformation::class);
    }

    public function file()
    {
        return $this->hasOne(File::class);
    }
}
