<?php

namespace App\Http\Requests\Api\Consultation;

use Illuminate\Foundation\Http\FormRequest;

class ManageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'symptoms' => ['required', 'string', 'max:255', 'min:4'],
            'date_symptoms' => ['date'],
            'comment' => ['string'],
            'pet_id' => ['required', 'integer'],
            'vet_id' => ['required', 'integer'],
            'vet_consultation_type_id' => ['required', 'integer'],
            'vet_service_id' => ['required', 'integer']
        ];
    }
}
