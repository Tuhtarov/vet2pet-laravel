<?php

namespace App\Services\Pet;

use App\Contracts\Pet\IPetProcedureManager;
use App\Http\Requests\Api\Pet\ManagePetRequest;
use App\Models\Pet;
use App\Models\PetProcedure;

class PetProcedureManager implements IPetProcedureManager
{
    public function getProceduresFrom(ManagePetRequest $request): array
    {
        $procedures = $request->input('procedures');
        $resultArray = [];

        if (!empty($procedures) && is_array($procedures)) {
            foreach ($procedures as $procedureData) {
                if (!empty($procedureData['id'])) {
                    $procedure = PetProcedure::findOrFail($procedureData['id']);
                } else {
                    $procedure = new PetProcedure($procedureData);
                }

                $resultArray[] = $procedure;
            }
        }

        return $resultArray;
    }

    /**
     * @param Pet $pet
     * @param PetProcedure[] $procedures
     * @return Pet
     */
    public function saveProceduresFor(Pet $pet, array $procedures): Pet
    {
        $this->validate($pet);

        foreach ($procedures as $procedure) {
            $procedure->pet_id = $pet->id;
            $procedure->save();
        }

        return $pet;
    }

    public function updateProceduresFor(Pet $pet, ManagePetRequest $request): Pet
    {
        $this->validate($pet);

        $procedures = $request->input('procedures');

        if (!empty($procedures) && is_array($procedures)) {
            foreach ($procedures as $procedureData) {
                if (!empty($procedureData['id'])) {
                    /** @var $procedure PetProcedure */
                    $procedure = PetProcedure::findOrFail($procedureData['id']);
                    $procedure->pet_id = $pet->id;
                    $procedure->updateOrFail($procedureData);
                    continue;
                }

                $procedure = new PetProcedure($procedureData);
                $procedure->pet_id = $pet->id;
                $procedure->saveOrFail();
            }
        }

        return $pet;
    }

    private function validate(Pet $pet): void
    {
        if (empty($pet->id)) {
            throw new \InvalidArgumentException('The pet must have id');
        }
    }
}
