<?php

namespace App\Contracts;

use App\Models\User;

interface IVetUpdater
{
    public function update(User $vet, array $data): User;
}
