<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_photos', function (Blueprint $table) {
            $table->id();

            $table->foreignId('pet_id');
            $table->foreign('pet_id')
                ->on('pets')->references('id');

            $table->foreignId('file_id');
            $table->foreign('file_id')
                ->on('files')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_photos');
    }
};
