<?php

namespace App\Http\Resources\Pet;

use App\Models\PetBreed;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PetBreed
 */
class PetBreedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            
            'pet_type_id' => $this->pet_type_id,
            'pet_type_name' => $this->petType?->name,
        ];
    }
}
