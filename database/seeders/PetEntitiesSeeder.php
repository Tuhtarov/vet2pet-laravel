<?php

namespace Database\Seeders;

use App\Models\DetentionCondition;
use App\Models\FoodBrand;
use App\Models\Gender;
use App\Models\Nutrition;
use App\Models\Origin;
use App\Models\PetBreed;
use App\Models\PetProcedureType;
use App\Models\PetType;
use App\Models\PlaceCondition;
use App\Models\Sterialization;
use Illuminate\Database\Seeder;

class PetEntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['сухой корм', 'мокрый корм', 'натуральный корм', 'смешанное кормление'] as $val) {
            $entity = new Nutrition();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['Город', 'Село'] as $val) {
            $entity = new PlaceCondition();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['помещение', 'улица'] as $val) {
            $entity = new DetentionCondition();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['заводчик', 'магазин', 'приют', 'знакомые', 'улица'] as $val) {
            $entity = new Origin();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['да', 'нет', 'неизвестно'] as $val) {
            $entity = new Sterialization();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['мужской', 'женский'] as $val) {
            $entity = new Gender();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['собачие', 'кошачие'] as $val) {
            $entity = new PetType();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['Алабай', 'Немецкая овчарка'] as $val) {
            $entity = new PetBreed();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['Pedigree', 'Kiti-Cat'] as $val) {
            $entity = new FoodBrand();
            $entity->name = $val;
            $entity->save();
        }

        foreach (['Комплексная вакцинация', 'Вакцинация от бешенства', 'Дегельминтизация',
                     'Обработка от блох и клещей', 'Посещение ветеринара'] as $val) {
            $entity = new PetProcedureType();
            $entity->name = $val;
            $entity->save();
        }
    }
}
