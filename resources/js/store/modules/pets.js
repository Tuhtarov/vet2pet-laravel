import {axios, axiosForUploading} from '@/api/Axios';

export default {
    state: {
        pets: [],
        myPets: [],
    },
    getters: {
        allPets: state => state.pets,
        myPets: state => state.myPets,

        getPet: (state, getters) => (petId) => {
            getters.allPets.forEach(pet => {
                if (pet.id === petId) {
                    return pet
                }
            })

            return null
        },

        getMyPet: (state, getters) => (petId) => {
            getters.myPets.forEach(pet => {
                if (pet.id === petId) {
                    return pet
                }
            })

            return null
        },
    },
    mutations: {
        setAllPets: (state, pets) => {
            state.pets = pets
        },
        setMyPets: (state, pets) => {
            state.myPets = pets
        },

        updatePet(state, pet) {
            state.pets = state.pets.reduce((p, item) => {
                if (item.id === pet.id) {
                    item = pet;
                }
                p.push(item);
                return p;
            }, [])
        },

        addPet: (state, pet) => {
            state.pets.push(pet);
        },
    },
    actions: {
        async fetchAllPets({commit, getters}) {
            return await axios.get('/pet')
                .then(({data}) => {
                    commit('setAllPets', data)
                    return getters.allPets;
                })
        },

        async fetchMyPets({commit, getters}) {
            return await axios.get('/pet/my')
                .then(({data}) => {
                    commit('setMyPets', data)
                    return getters.myPets;
                })
        },

        async create({commit}, params) {
            await axiosForUploading.post(`/pet`, params)
                .then(({data}) => {
                    commit('addPet', data)
                    return data
                })
        },

        async update({commit}, params) {
            await axiosForUploading.post(`/pet/update/${params.get('id')}`, params)
                .then(({data}) => {
                    return data
                })
        },

        async fetchPetById({commit, getters}, id) {
            return await axios.get(`/pet/${id}`).then(({data}) => {
                commit('updatePet', data)
                return data
            })
        }
    },
    namespaced: true,
}
