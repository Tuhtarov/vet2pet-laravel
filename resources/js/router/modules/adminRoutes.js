import {setLayout, setNavLink} from "@/router/metaUtils";
import Home from "@/views/Home.vue";

import AllReviews from "@/views/admin/reviews/All.vue"
import ClientSettings from "@/views/client/ClientSettings.vue"

import petAdditionalRoutes from "@/router/modules/admin/petAdditionalRoutes";
import consultationRoutes from "@/router/modules/admin/consultationRoutes";
import serviceRoutes from "@/router/modules/admin/serviceRoutes";
import petRoutes from "@/router/modules/admin/petRoutes";
import userRoutes from "@/router/modules/admin/userRoutes";

export default [
    ...petAdditionalRoutes,
    ...consultationRoutes,
    ...serviceRoutes,
    ...petRoutes,
    ...userRoutes,

    {
        path: '/all-reviews',
        name: 'all-reviews',
        component: AllReviews,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Отзывы",
                icon: "mdi-star-circle"
            }),
            role: 'admin'
        }
    },

    {
        path: '/stats',
        name: 'stats',
        component: Home,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Статистика",
                icon: "mdi-chart-timeline-variant-shimmer"
            }),
            role: 'admin'
        }
    },

    {
        path: '/settings',
        name: 'settings',
        component: ClientSettings,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Настройки",
                icon: "mdi-cog"
            }),
            role: 'admin'
        }
    },
]
