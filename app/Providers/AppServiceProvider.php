<?php

namespace App\Providers;

use App\Contracts\{IClientCreator,
    IFileUploader,
    IUserRegister,
    IVetCreator,
    IVetUpdater,
    Pet\IPetHealthManager,
    Pet\IPetProcedureManager};
use App\Services\{ClientCreator,
    FileUploader,
    Pet\PetHealthManager,
    Pet\PetProcedureManager,
    UserRegister,
    VetCreator,
    VetUpdater};
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        JsonResource::withoutWrapping();
        $this->app->bind(IFileUploader::class, FileUploader::class);

        $this->app->bind(IVetCreator::class, VetCreator::class);
        $this->app->bind(IClientCreator::class, ClientCreator::class);
        $this->app->bind(IUserRegister::class, UserRegister::class);
        $this->app->bind(IVetUpdater::class, VetUpdater::class);

        $this->app->bind(IPetProcedureManager::class, PetProcedureManager::class);
        $this->app->bind(IPetHealthManager::class, PetHealthManager::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
