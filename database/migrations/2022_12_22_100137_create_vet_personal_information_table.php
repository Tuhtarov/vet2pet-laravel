<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_personal_information', function (Blueprint $table) {
            $table->id();
            $table->text('address');
            $table->unsignedSmallInteger('passport_series');
            $table->unsignedInteger('passport_number');
            $table->boolean('is_activity')->default(false);

            $table->foreignId('vet_work_type_id');
            $table->foreign('vet_work_type_id')
                ->on('vet_work_types')->references('id');

            $table->foreignId('user_id');
            $table->foreign('user_id')
                ->on('users')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vet_personal_information');
    }
};
