<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PetType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|PetType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PetType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read PetType|null $breeds
 * @property-read int|null $breeds_count
 */
class PetType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }

    public function breeds()
    {
        return $this->hasMany(PetBreed::class, 'pet_type_id', 'id');
    }
}
