import {setLayout, setNavLink} from "@/router/metaUtils";
import My from "@/views/vet/reviews/My.vue";

export default [
    {
        path: '/reviews',
        name: 'reviews',
        component: My,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Мои отзывы",
                icon: "mdi-star-circle"
            }),
            role: 'vet'
        }
    },
];
