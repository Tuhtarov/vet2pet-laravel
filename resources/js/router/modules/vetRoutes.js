import {setLayout, setNavLink} from "@/router/metaUtils";
import VetSettings from "@/views/vet/VetSettings.vue"
import consultationRoutes from "@/router/modules/vet/consultationRoutes";
import reviewRoutes from "@/router/modules/vet/reviewRoutes";

export default [
    ...consultationRoutes,
    ...reviewRoutes,

    {
        path: '/settings',
        name: 'settings',
        component: VetSettings,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Настройки",
                icon: "mdi-cog"
            }),
            role: 'vet'
        }
    },
]
