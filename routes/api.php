<?php

use App\Http\Controllers\PackageController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\Api\{Client\Consultation\AttachmentController,
    Client\Consultation\ConsultationController as ClientConsultationController,
    EducationController,
    GenderController,
    MessageController,
    PersonalController,
    Pet\DetentionConditionController,
    Pet\FoodBrandController,
    Pet\NutritionController,
    Pet\OriginController,
    Pet\PetBreedController,
    Pet\PetController,
    Pet\PetTypeController,
    Pet\PlaceConditionController,
    Pet\Procedure\ProcedureController,
    Pet\Procedure\ProcedureTypeController,
    Pet\SterializationController,
    Role\RoleController,
    User\AdminUserController,
    UserController,
    Vet\ConsultationController,
    Vet\ConsultationResponseController,
    Vet\ConsultationTypeController,
    Vet\FileController,
    Vet\VetServiceController,
    VetController,
    WorkTypeController};
use App\Http\Controllers\Auth\{NewPasswordController, RegisterController};
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return (new UserResource($request->user()));
});

Route::middleware('auth')->group(function () {
    Route::prefix('user')->group(function () {
        Route::post('/password-update', [NewPasswordController::class, 'update']);
        Route::post('/update', [UserController::class, 'update']);
        Route::post('{user}/updateToken', [UserController::class, 'updateToken']);
        Route::get('/vets', [UserController::class, 'vets']);

        Route::get('/all', [AdminUserController::class, 'index']);
        Route::get('/{user}', [AdminUserController::class, 'show'])->whereNumber('user');
        Route::post('/admin-store', [AdminUserController::class, 'store'])->name('user.admin-store');
        Route::post('/admin-update/{user}', [AdminUserController::class, 'update'])
            ->whereNumber('user')
            ->name('user.admin-update');
    });

    Route::get('/packages', [PackageController::class, 'index']);

    Route::prefix('role')->group(function () {
        Route::get('/', [RoleController::class, 'index']);
    });

    Route::prefix('chat')->group(function() {
        Route::get('/messages',  [MessageController::class, 'index']);
        Route::post('/send', [MessageController::class, 'send']);
    });

    Route::prefix('gender')->group(function () {
        Route::get('/', [GenderController::class, 'index']);
    });

    Route::post('/payment', [PaymentController::class, 'create']);

    Route::prefix('education')->group(function () {
        Route::post('/{id}/add-images', [EducationController::class, 'addEducationImages']);

        Route::post('/{id}', [VetController::class, 'updateEducation'])
            ->whereNumber('id');

        Route::post('/delete-file/{id}', [EducationController::class, 'deleteEducationFile'])
            ->whereNumber('id');

        Route::get('/{id}', [EducationController::class, 'show'])
            ->whereNumber('id');
    });

    Route::prefix('vet')->group(function () {
        Route::get('/workTypes', [WorkTypeController::class, 'workTypes']);
        Route::post('/register', [RegisterController::class, 'vetRegister']);
    });

    Route::prefix('vet-service')->group(function () {
        Route::post('/', [VetServiceController::class, 'store']);
        Route::get('/', [VetServiceController::class, 'index']);
        Route::get('/{service}', [VetServiceController::class, 'show'])->whereNumber('service');
        Route::put('/{service}', [VetServiceController::class, 'update'])->whereNumber('service');
    });

    Route::prefix('pet')->group(function () {
        Route::put('/change-photo/{pet}', [PetController::class, 'savePhotos'])->whereNumber('pet');
        Route::post('/delete-photo/{photo}', [PetController::class, 'deletePhoto'])->whereNumber('id');
        Route::put('/set-current-photo/{pet}', [PetController::class, 'setCurrentPhoto'])->whereNumber('pet');
        Route::get('/my', [PetController::class, 'myPets']);
        Route::post('/update/{pet}', [PetController::class, 'update'])->whereNumber('pet');
    });

    Route::prefix('pet-procedure')->group(function () {
        Route::get('', [ProcedureController::class, 'index']);
        Route::delete('/{procedure}', [ProcedureController::class, 'delete'])->whereNumber('procedure');
    });

    Route::prefix('pet-procedure-type')->group(function () {
        Route::get('', [ProcedureTypeController::class, 'index']);
    });

    Route::apiResource('pet', PetController::class);

    Route::apiResource('consultation-type', ConsultationTypeController::class);

    Route::prefix('consultation')->group(function () {
        Route::get('/my', [ConsultationController::class, 'myAll']);
        Route::get('/my/{consultation}', [ConsultationController::class, 'my'])->whereNumber('consultation');
    });

    Route::prefix('client-consultation')->group(function () {
        Route::get('/', [ClientConsultationController::class, 'index']);
        Route::get('/by-pet/{pet}', [ClientConsultationController::class, 'index']);

        Route::post('/', [ClientConsultationController::class, 'create']);
        Route::get('/{consultation}', [ClientConsultationController::class, 'show'])->whereNumber('consultation');
        Route::post('/update/{consultation}', [ClientConsultationController::class, 'update'])->whereNumber('consultation');
        Route::delete('/{consultation}', [ClientConsultationController::class, 'delete'])->whereNumber('consultation');
        Route::get('/{consultation}/messages', [ClientConsultationController::class, 'getMessages'])->whereNumber('consultation');
        Route::post('/{consultation?}/send', [ClientConsultationController::class, 'send'])->whereNumber('consultation');
        Route::post('/sendFCM' ,[ClientConsultationController::class, 'sendF'])->whereNumber('consultation');
    });

    Route::prefix('client-consultation-attachment')->group(function () {
        Route::delete('/{attachment}', [AttachmentController::class, 'delete'])->whereNumber('attachment');
    });

    Route::apiResource('consultation', ConsultationController::class);

    Route::delete('consultation/file/{attachment}', [FileController::class, 'delete']);

    Route::apiResource('consultation-response', ConsultationResponseController::class);
    Route::patch('consultation-response/{consultationResponse}/change/status', [ConsultationResponseController::class, 'changeStatus']);
    Route::apiResource('pet-type', PetTypeController::class);
    Route::apiResource('detention', DetentionConditionController::class);
    Route::apiResource('place-condition', PlaceConditionController::class);
    Route::apiResource('food-brand', FoodBrandController::class);

    Route::apiResource('pet-breed', PetBreedController::class);
    Route::prefix('pet-breed')->group(function () {
        Route::get('by-pet-type/{petType}', [PetBreedController::class, 'allByPetType'])->whereNumber('petType');
    });

    Route::apiResource('nutrition', NutritionController::class);
    Route::apiResource('origin', OriginController::class);
    Route::apiResource('sterialization', SterializationController::class);
});

Route::middleware('auth')->prefix('personal')->group(function () {
    Route::post('/update', [PersonalController::class, 'update']);

    Route::post('/{userId}/update', [PersonalController::class, 'update'])
        ->whereNumber('userId');
});

Route::get('/payment/success', [PaymentController::class, 'success'])->name('payment.success');
Route::match(['GET', 'POST'], '/payment/callback', [PaymentController::class, 'callback'])->name('payment.callback');

Route::prefix('client')->group(function () {
    Route::post('/register', [RegisterController::class, 'clientRegister'])->name('clientRegister');
});

