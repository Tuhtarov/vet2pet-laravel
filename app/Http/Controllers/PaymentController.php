<?php

namespace App\Http\Controllers;

use App\Enums\TransactionStatusEnum;
use App\Models\Package;
use App\Models\Transaction;
use App\Models\User;
use App\Services\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use YooKassa\Model\Notification\NotificationEventType;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;

class PaymentController extends Controller
{
    public function create(Request $request, PaymentService $paymentService)
    {
        $packageId = +$request->input('package_id');
        $package = Package::query()->findOrFail($packageId);

        $amount = $package->price;
        $description = 'Пополнение';

        $transaction = new Transaction();
        $transaction->price = $amount;
        $transaction->description = $description;
        $transaction->save();

        if ($transaction) {
            $link = $paymentService->createPayment($amount, $description, [
                'transaction_id' => $transaction->id,
                'package_id' => $package->id,
                'user_id' => $request->user()->id
            ]);

            return response()->json(['link' => $link]);
        }
    }

    public function callback(Request $request, PaymentService $paymentService)
    {
        $source = file_get_contents('php://input');
        $requestBody = json_decode($source, true);

        $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($requestBody)
            : new NotificationWaitingForCapture($requestBody);

        $payment = $notification->getObject();

        if (isset($payment->status) && $payment->status === 'succeeded') {
            if ((bool) $payment->paid === true) {
                $metadata = (object) $payment->metadata;
                if (isset($metadata->transaction_id) && isset($metadata->package_id)) {
                    $transaction = Transaction::find(+$metadata->transaction_id);
                    $transaction->status = TransactionStatusEnum::CONFIRMED;
                    $transaction->save();

                    $package = Package::find(+$metadata->package_id);
                    $user = User::find(+$metadata->user_id);
                    $user->count_package += $package->count;
                    $user->last_date_package = now()->modify('+3 month');
                    $user->save();
                }
            }
        }
    }

    public function success()
    {
        return 'Платёж прошёл успешно вернитесь в ваше приложение';
    }
}
