<?php

namespace App\Http\Requests\Api\Client\Consultation;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "symptoms" => ['sometimes', 'string', 'max:255', 'min:4'],
            "date_symptoms" => ['sometimes', 'date'],
            "comment" => ['sometimes', 'string', 'nullable'],
            "pet_id" => ['sometimes', 'integer'],
            "vet_id" => ['nullable', 'sometimes', 'integer'],
            "client_id" => ['sometimes', 'integer'],
            "term" => ['nullable', 'sometimes', 'date'],
        ];
    }
}
