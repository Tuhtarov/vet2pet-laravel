<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');
//
//Route::get('/', [HomeController::class, 'index'])->name('home');
//Route::get('/home', [HomeController::class, 'index'])->name('home');
//Route::post('/client-register', [RegisterController::class, 'clientRegister'])->name('clientRegister');
//Route::post('/vet-register', [RegisterController::class, 'vetRegister'])->name('vetRegister');
//
//Route::get('/login', function () {
//    return view('login');
//});
//
//Route::get('/register', function () {
//    return view('register');
//});

Auth::routes();
