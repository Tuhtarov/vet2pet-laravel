<?php

namespace App\Services;

use App\Contracts\IUserRegister;
use App\Contracts\IVetUpdater;
use App\Models\User;
use App\Models\VetBankDetail;
use App\Models\VetPersonalInformation;
use Illuminate\Support\Facades\DB;

class VetUpdater extends IUserRegister implements IVetUpdater
{
    public function update(User $vet, array $data): User
    {
        return DB::transaction(function () use ($data, $vet) {
            // сначала обновляем основные данные
            if (isset($data['password'])) {
                $data = $this->register($data, 'vet');
            }

            $vet->update($data);

            // далее связанные сущности
            $this->updatePersonalInfo($data);
            $this->updateBankDetails($data);
            $this->updateEducation($data);

            return User::findOrFail($vet->id);
        });
    }

    private function updateBankDetails(array $data): bool
    {
        $bank = [];
        $bank['settlement_account'] = $data['settlement_account'];
        $bank['bank_name'] = $data['bank_name'];
        $bank['bank_address'] = $data['bank_address'];
        $bank['bank_correspondent_account'] = $data['bank_correspondent_account'];
        $bank['bik'] = $data['bik'];

        $bankEntity = VetBankDetail::findOrFail($data['bank_id']);
        return $bankEntity->update($bank);
    }

    private function updatePersonalInfo(array $data): bool
    {
        $info = [];
        $info['vet_work_type_id'] = $data['vet_work_type_id'];
        $info['address'] = $data['address'];
        $info['passport_series'] = $data['passport_series'];
        $info['passport_number'] = $data['passport_number'];
        $entity = VetPersonalInformation::findOrFail($data['vet_personal_information_id']);
        return $entity->update($info);
    }

    private function updateEducation(array $data): bool
    {
        return true;
    }
}
