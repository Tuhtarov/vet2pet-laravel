import axios from '@/api/Axios';
export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        async fetchEducationById(store, id) {
            return await axios.get(`/education/${id}`)
                .then(({data}) => data)
        },

        async deleteEducationFile(store, id) {
            return await axios.post(`/education/delete-file/${id}`)
                .then(({data}) => data)
        },
    },
    namespaced: true,
}
