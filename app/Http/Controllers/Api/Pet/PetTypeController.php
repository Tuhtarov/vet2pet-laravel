<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\PetTypeResource;
use App\Models\PetType;
use Illuminate\Http\Request;

class PetTypeController extends Controller
{
    public function index()
    {
        return PetTypeResource::collection(
            PetType::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return PetTypeResource::make(
            PetType::create($data)
        );
    }

    public function show(PetType $petType)
    {
        return PetTypeResource::make($petType);
    }

    public function update(Request $request, PetType $petType)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $petType->update($data);

        return PetTypeResource::make($petType);
    }
}
