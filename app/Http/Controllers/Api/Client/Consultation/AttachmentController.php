<?php

namespace App\Http\Controllers\Api\Client\Consultation;

use App\Http\Controllers\Controller;
use App\Models\Pet;
use App\Models\VetConsultation;
use App\Models\VetConsultationAttachment;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    public function delete(VetConsultationAttachment $attachment)
    {
        /**
         * @var VetConsultation $cons
         */
        $cons = VetConsultation::findOrFail($attachment->vet_consultation_id);
        $usId = (int)request()->user()->id;

        /* вложение принадлежит текущему пользователю? (проверка через консультацию) */
        $isAttachmentByCurrentUser = (int)$cons->anonymous_pet_owner_id === $usId || $cons->pet?->user_id === $usId;

        if ($isAttachmentByCurrentUser) {
            return response()->json(['result' => $attachment->deleteOrFail()]);
        }

        throw new \InvalidArgumentException('Нельзя удалить чужое вложение.');
    }
}
