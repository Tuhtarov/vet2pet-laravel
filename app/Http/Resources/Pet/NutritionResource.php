<?php

namespace App\Http\Resources\Pet;

use App\Models\Nutrition;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Nutrition
 */
class NutritionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
