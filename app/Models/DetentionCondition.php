<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DetentionCondition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition query()
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DetentionCondition whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $pets_count
 */
class DetentionCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }
}
