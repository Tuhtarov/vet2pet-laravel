<?php

namespace Database\Seeders;

use App\Models\VetWorkType;
use Illuminate\Database\Seeder;

class VetEntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Самозанятый', 'Индивидуальный предприниматель', 'Физическое лицо'] as $val) {
            $entity = new VetWorkType();
            $entity->name = $val;
            $entity->save();
        }
    }
}
