export const setLayout = (name, role = null) => ({
    layout: `${name}Layout`,
    role: role ?? name
});

export const setNavLink = linkInfo => ({
    navLink: linkInfo
});
