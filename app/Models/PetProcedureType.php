<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PetProcedureType
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Query\Builder|PetProcedureType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedureType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|PetProcedureType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PetProcedureType withoutTrashed()
 */
class PetProcedureType extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];
}
