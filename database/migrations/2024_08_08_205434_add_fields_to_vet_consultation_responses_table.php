<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vet_consultation_responses', function (Blueprint $table) {
            $table->enum('status', [1, 2, 3, 4])->default(1); // 1-не утвержден, 2-отправлен на согласование, 3-откланен, 4-согласован
            $table->text('reason_deviation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vet_consultation_responses', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('reason_deviation');
        });
    }
};
