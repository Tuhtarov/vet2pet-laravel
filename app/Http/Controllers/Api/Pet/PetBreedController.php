<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\PetBreedResource;
use App\Models\PetBreed;
use App\Models\PetType;
use Illuminate\Http\Request;

class PetBreedController extends Controller
{
    public function index()
    {
        return PetBreedResource::collection(
            PetBreed::all()
        );
    }

    public function allByPetType(PetType $petType)
    {
        return PetBreedResource::collection($petType->breeds);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return PetBreedResource::make(
            PetBreed::create($data)
        );
    }

    public function show(PetBreed $entity)
    {
        return PetBreedResource::make($entity);
    }

    public function update(Request $request, PetBreed $entity)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $entity->update($data);

        return PetBreedResource::make($entity);
    }
}
