import {setLayout, setNavLink} from "@/router/metaUtils";
import AllUsers from "@/views/admin/users/AllUsers.vue";
import Form from "@/views/admin/users/Form.vue";
import Show from "@/views/admin/users/Show.vue";

export default [
    {
        path: '/users',
        name: 'users',
        component: AllUsers,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Пользователи",
                icon: "mdi-account-multiple-outline"
            }),
            role: 'admin'
        }
    },

    {
        path: '/user-edit/:id',
        name: 'user-edit',
        component: Form,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/new-user/',
        name: 'user-add',
        component: Form,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/user/:id',
        name: 'user-show',
        component: Show,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
]
