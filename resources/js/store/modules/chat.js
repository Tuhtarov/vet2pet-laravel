import axios from '@/api/Axios';

export default {
    state: {
        messages: []
    },
    getters: {
        all: state => state.messages,
    },
    mutations: {
        setAll: (state, data) => {
            state.messages = data
        },

        add: (state, data) => {
            state.messages.push(data);
        },
    },
    actions: {
        async getMessages({commit}, id) {
            return await axios.get(`/client-consultation/${id}/messages`)
                .then(({data}) => {
                    commit('setAll', data);

                    return data;
                })
        },

        async addMessage({commit}, chat) {
            return await axios.post(`/client-consultation/${chat.id}/send`, {message: chat.message})
                .then((res) => {
                    return res;
                })
        },
    },
    namespaced: true,
}
