import {setLayout} from "@/router/metaUtils";

import PetTypes from "@/views/admin/pets/PetTypes.vue"
import PetBreeds from "@/views/admin/pets/PetBreeds.vue"
import PetDetentionConditions from "@/views/admin/pets/PetDetentionConditions.vue"
import PetPlaceConditions from "@/views/admin/pets/PetPlaceConditions.vue"
import FoodBrands from "@/views/admin/pets/FoodBrands.vue"
import PetNutrition from "@/views/admin/pets/PetNutrition.vue"
import PetOrigins from "@/views/admin/pets/PetOrigins.vue"

export default [
    {
        path: '/pet-types',
        name: 'pet-types',
        component: PetTypes,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/pet-breeds',
        name: 'pet-breeds',
        component: PetBreeds,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/detention-conditions',
        name: 'detention-conditions',
        component: PetDetentionConditions,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/place-conditions',
        name: 'place-conditions',
        component: PetPlaceConditions,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/food-brands',
        name: 'food-brands',
        component: FoodBrands,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/pet-nutrition',
        name: 'pet-nutrition',
        component: PetNutrition,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
    {
        path: '/pet-origins',
        name: 'pet-origins',
        component: PetOrigins,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
]
