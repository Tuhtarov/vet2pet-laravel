<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class NewPasswordController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'current_password' => 'required|current_password:web',
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        $user = $request->user();
        $user->password = Hash::make($request->get('password'));

        return response()->json($user->save());
    }
}
