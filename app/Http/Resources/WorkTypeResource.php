<?php

namespace App\Http\Resources;

use App\Models\VetWorkType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetWorkType
 */
class WorkTypeResource extends JsonResource
{
    public static $wrap = 'workType';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
