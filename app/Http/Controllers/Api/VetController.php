<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EducationResource;
use App\Models\User;
use App\Models\VetEducation;
use Illuminate\Http\Request;

class VetController extends Controller
{
    public function index()
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function updateEducation(Request $request, int $id)
    {
        $request->validate([
            'name' => ['required', 'string']
        ]);

        $education = VetEducation::findOrFail($id);
        $education->name = $request->get('name');
        $education->save();

        return new EducationResource($education);
    }

    public function destroy($id)
    {
        //
    }
}
