<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_procedures', function (Blueprint $table) {
            $table->id();
            $table->date('procedure_date');
            $table->text('reason')->nullable();

            /* стериализация */
            $table->foreignId('pet_id')->nullable();
            $table->foreign('pet_id')->on('pets')->references('id');

            /* владелец (пользователь) */
            $table->foreignId('procedure_type_id');
            $table->foreign('procedure_type_id')
                ->on('pet_procedure_types')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_procedures');
    }
};
