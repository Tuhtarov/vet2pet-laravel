<?php

namespace App\Http\Controllers\Api;

use App\Events\MessageSent;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        return Message::query()->with('user')->get();
    }

    public function send(Request $request)
    {
        $message = $request->user()->messages()->create($request->validate([
            'message' => 'required|string'
        ]));

        broadcast(new MessageSent($request->user(), $message));

        return $message;
    }
}
