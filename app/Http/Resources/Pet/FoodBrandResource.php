<?php

namespace App\Http\Resources\Pet;

use App\Models\FoodBrand;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin FoodBrand
 */
class FoodBrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
