<?php

namespace App\Http\Controllers\Api\Vet;

use App\Contracts\IFileUploader;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client\Consultation\CreateRequest;
use App\Http\Requests\Api\Client\Consultation\UpdateRequest;
use App\Http\Requests\Api\Consultation\ManageRequest;
use App\Http\Resources\Vet\ConsultationResource;
use App\Models\Message;
use Illuminate\Support\Facades\Log;
use App\Models\Pet;
use App\Models\Role;
use App\Models\User;
use App\Models\VetConsultation;
use App\Models\VetConsultationAttachment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orhanerday\OpenAi\OpenAi;

class ConsultationController extends Controller
{
    public function __construct(private readonly IFileUploader $uploader)
    {
    }

    public function index(Request $request)
    {
        $consultationQuery = VetConsultation::query();
        /** @var User $user */
        $user = Auth::user();
        /** @var Role $role */
        $role = $user->role;

        if ($petId = $request->input('pet_id')) {
            $consultationQuery->wherePetId($petId);
        }

        if ($role->slug === Role::CLIENT_SLUG) {
            $consultationQuery->where('client_id', $user->id);
        }

        return ConsultationResource::collection(
            $consultationQuery->get()
        );
    }

    public function myAll(Request $request)
    {
        $userId = $request->user()->id;

        return ConsultationResource::collection(
            VetConsultation::whereVetId($userId)->get()
        );
    }

    public function my(int $consultation)
    {
        $userId = request()->user()->id;

        return ConsultationResource::make(
            VetConsultation::whereKey($consultation)->firstOrFail()
        );
    }

    public function store(CreateRequest $request)
    {
        $consultationData = $request->validated();

        $petId = $request->input('pet_id');
        $petBelongsToCurrentUser = !empty($petId) && in_array((int)$petId, $this->getClientPetIds());

        if ($petBelongsToCurrentUser || is_null($petId))
            $consultation = new VetConsultation($consultationData);

        if (isset($consultation)) {
            $consultation->client_id = request()->user()->id;

            try {
                $open_ai_key = getenv('OPENAI_API_KEY');
                $open_ai = new OpenAi($open_ai_key);
                $chat = $open_ai->chat([
                    'model' => 'gpt-3.5-turbo',
                    'messages' => [
                        [
                            "role" => "system",
                            "content" => "Какие заболевания можно определить по данным симптомам у животного?"
                        ],
                    ],
                    'temperature' => 1.0,
                    'max_tokens' => 4000,
                    'frequency_penalty' => 0,
                    'presence_penalty' => 0,
                ]);
                $answer = json_decode($chat);
                Log::error($chat);
                $consultation->commentChat = $answer->choices[0]->message->content;
            } catch (\Throwable $exception) {
                Log::error($exception);
            }
            $consultation->saveOrFail();

            $this->saveFiles($request, $consultation);
            return ConsultationResource::make($consultation);
        }

        throw new \InvalidArgumentException(
            "Для консультации был выбран питомец (ID: $petId), принадлежащий другому пользователю."
        );
    }

    public function show(VetConsultation $consultation)
    {
        return ConsultationResource::make($consultation);
    }

    public function update(VetConsultation $consultation, UpdateRequest $request)
    {
        if (empty($consultation->pet_id) || empty($request->validated()['pet_id']))
            $consultation->client_id = request()->user()->id;

        if ($consultation->updateOrFail($request->validated())) {
            $this->saveFiles($request, $consultation);
        }
        return ConsultationResource::make(
            VetConsultation::find($consultation->id)
        );
    }

    public function destroy(VetConsultation $consultation)
    {
        try {
            if ($consultation->response) {
                throw new \Exception('Нельзя удалить консультацию в которой есть ответ ветеренара');
            }

            $consultation->delete();

            return ConsultationResource::make($consultation);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 400,
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    private function saveFiles(FormRequest $request, VetConsultation $consultation): void
    {
        $files = $request->files->all('attachments');

        if (!empty($files)) {
            foreach ($files as $file) {
                VetConsultationAttachment::create([
                    'vet_consultation_id' => $consultation->id,
                    'file_id' => $this->uploader->handle($file)->id
                ]);
            }
        }
    }

    /**
     * @return array<int>
     */
    private function getClientPetIds(): array
    {
        return array_map(
            fn(Pet $pet): int => (int)$pet->id,
            Pet::whereUserId(request()->user()->id)->get()->all()
        );
    }
}
