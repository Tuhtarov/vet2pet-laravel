<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetBankDetail
 *
 * @property int $id
 * @property string $bank_name
 * @property string $bank_address
 * @property int $bank_correspondent_account
 * @property int $settlement_account
 * @property int $bik
 * @property int $vet_personal_information_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\VetPersonalInformation $vetPersonalInformation
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereBankCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereSettlementAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetBankDetail whereVetPersonalInformationId($value)
 * @mixin \Eloquent
 */
class VetBankDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'bank_name',
        'bank_address',
        'bank_correspondent_account',
        'settlement_account',
        'bik',
        'vet_personal_information_id',
    ];

    public function vetPersonalInformation()
    {
        return $this->belongsTo(VetPersonalInformation::class);
    }
}
