<?php

namespace App\Services;

use App\Contracts\IUserRegister;
use App\Contracts\IVetCreator;
use App\Models\User;
use App\Models\VetBankDetail;
use App\Models\VetEducation;
use App\Models\VetPersonalInformation;
use Illuminate\Support\Facades\DB;

class VetCreator extends IUserRegister implements IVetCreator
{
    public function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            $user = new User($this->register($data, 'vet'));
            $user->save();

            $personalInfo = $this->preparePersonalInfo($data);
            $personalInfo->user_id = $user->id;
            $personalInfo->save();

            $bankDetails = $this->prepareBankDetails($data);
            $bankDetails->vet_personal_information_id = $personalInfo->id;
            $bankDetails->save();

            $education = $this->prepareEducation($data);
            $education->vet_personal_information_id = $personalInfo->id;
            $education->save();

            return $user;
        });
    }

    private function prepareBankDetails(array $data): VetBankDetail
    {
        $bank = [];
        $bank['settlement_account'] = $data['settlement_account'];
        $bank['bank_name'] = $data['bank_name'];
        $bank['bank_address'] = $data['bank_address'];
        $bank['bank_correspondent_account'] = $data['bank_correspondent_account'];
        $bank['bik'] = $data['bik'];

        return new VetBankDetail($bank);
    }

    private function preparePersonalInfo(array $data): VetPersonalInformation
    {
        $info = [];
        $info['vet_work_type_id'] = $data['vet_work_type_id'];
        $info['address'] = $data['address'];
        $info['passport_series'] = $data['passport_series'];
        $info['passport_number'] = $data['passport_number'];
        $info['user_id'] = null;
        $info['is_activity'] = false;

        return new VetPersonalInformation($info);
    }

    private function prepareEducation(array $data): VetEducation
    {
        $info = [];
        $info['name'] = $data['education_name'];

        return new VetEducation($info);
    }
}
