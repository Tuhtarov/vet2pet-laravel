<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\OriginResource;
use App\Models\Origin;
use Illuminate\Http\Request;

class OriginController extends Controller
{
    public function index()
    {
        return OriginResource::collection(
            Origin::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return OriginResource::make(
            Origin::create($data)
        );
    }

    public function show(Origin $entity)
    {
        return OriginResource::make($entity);
    }

    public function update(Request $request, Origin $entity)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $entity->update($data);

        return OriginResource::make($entity);
    }
}
