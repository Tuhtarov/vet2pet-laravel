<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\FoodBrandResource;
use App\Models\FoodBrand;
use Illuminate\Http\Request;

class FoodBrandController extends Controller
{
    public function index()
    {
        return FoodBrandResource::collection(
            FoodBrand::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return FoodBrandResource::make(
            FoodBrand::create($data)
        );
    }

    public function show(FoodBrand $food)
    {
        return FoodBrandResource::make($food);
    }

    public function update(Request $request, FoodBrand $food)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $food->update($data);

        return FoodBrandResource::make($food);
    }
}
