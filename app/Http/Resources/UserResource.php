<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class UserResource extends JsonResource
{
    public static $wrap = 'user';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'patronymic' => $this->patronymic,
            'initials' => mb_strtoupper(mb_substr($this->name, 0, 1)  . mb_substr($this->surname, 0, 1)),
            'fullName' => $this->getFullName(),
            'phone' => $this->phone,
            'birthday' => $this->birthday?->format('Y-m-d'),
            'birthday_timestamp' => $this->birthday?->timestamp,
            'city' => $this->city,
            'email' => $this->email,
            'created' => $this->created_at?->format('Y-m-d H:i:s'),

            'role' => $this->role->name,
            'role_slug' => $this->role->slug,
            'role_id' => $this->role_id,

            'personal' => $this->vetPersonal ? (new VetPersonalResource($this->vetPersonal)) : null,
            'count_package' => $this->count_package,
            'last_date_package' => $this->last_date_package
        ];
    }
}
