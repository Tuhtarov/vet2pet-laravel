<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PlaceCondition
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pet[] $pets
 * @property-read int|null $pets_count
 * @method static \Illuminate\Database\Eloquent\Builder|PlaceCondition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlaceCondition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlaceCondition query()
 * @method static \Illuminate\Database\Eloquent\Builder|PlaceCondition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlaceCondition whereName($value)
 * @mixin \Eloquent
 */
class PlaceCondition extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }
}
