<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetPhoto
 *
 * @property int $id
 * @property int $file_id
 * @property int $vet_personal_information_id
 * @property-read \App\Models\File $file
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetPhoto whereVetPersonalInformationId($value)
 * @mixin \Eloquent
 */
class VetPhoto extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'file_id',
        'vet_personal_information_id',
    ];

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
