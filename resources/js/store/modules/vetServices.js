import axios from '@/api/Axios';

export default {
    state: {
        services: [],
    },
    getters: {
        all: state => state.services,
    },
    mutations: {
        setAll: (state, services) => {
            state.services = services
        },

        add: (state, data) => {
            state.services.push(data);
        },
    },
    actions: {
        async fetchAll({commit}) {
            return await axios.get("/vet-service")
                .then(res => {
                    commit('setAll', res.data)
                    return res
                })
        },

        async create({commit}, entity) {
            return await axios.post("/vet-service", entity)
                .then(res => {
                    commit('add', res.data)
                    return res
                })
        },

        async update({commit, dispatch}, {entity, id}) {
            return await axios.put(`/vet-service/${id}`, entity)
                .then(res => {
                    console.log(res.data);
                    dispatch('fetchAll')
                    return res
                })
        },

        async fetchById(_, id) {
            return await axios.get(`/vet-service/${id}`).then(res => res.data)
        },
    },
    namespaced: true,
}
