<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Nutrition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Nutrition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Nutrition extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }
}
