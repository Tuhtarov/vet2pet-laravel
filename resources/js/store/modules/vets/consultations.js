import axios from '@/api/Axios';

export default {
    state: {
        consultations: [],
        myAll: []
    },
    getters: {
        all: state => state.consultations,
        myAll: state => state.myAll,
    },
    mutations: {
        setAll: (state, data) => {
            state.consultations = data
        },

        setMyAll: (state, data) => {
            state.myAll = data
        },

        add: (state, data) => {
            state.consultations.push(data);
        },
    },
    actions: {
        async fetchAll({commit}) {
            return await axios.get("/consultation").then(res => {
                commit('setAll', res.data)
                return res
            })
        },

        async fetchMyAll({commit}) {
            return await axios.get("/consultation/my").then(res => {
                commit('setMyAll', res.data)
                return res
            })
        },

        async fetchMy({commit}, id) {
            return await axios.get(`/consultation/my/${id}`).then(res => {
                return res.data
            })
        },

        async fetch({commit}, id) {
            return await axios.get(`/consultation/${id}`).then(res => {
                return res.data
            })
        },

        async create({commit}, entity) {
            return await axios.post("/consultation", entity).then(res => {
                commit('add', res.data)
                return res.data
            })
        },

        async update({commit, dispatch}, entity) {
            return await axios.put(`/consultation/${entity.id}`, entity).then(res => {
                dispatch('fetchAll')
                return res.data
            })
        },
    },
    namespaced: true,
}
