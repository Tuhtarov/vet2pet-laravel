<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vet_consultations', function (Blueprint $table) {
            $table->foreignId('anonymous_pet_owner_id')->nullable();
            $table->foreign('anonymous_pet_owner_id')
                ->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vet_consultations', function (Blueprint $table) {
            $table->dropForeign('vet_consultations_anonymous_pet_owner_id_foreign');
            $table->dropColumn('anonymous_pet_owner_id');
        });
    }
};
