import axios from '@/api/Axios';

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        async updatePassport(store, {user_id, passport}) {
            return await axios.post(`/personal/${user_id}/update`, passport)
        },

        async updateMyPassport(store, passport) {
            return await axios.post(`/personal/update`, passport)
        },
    },
    namespaced: true,
}
