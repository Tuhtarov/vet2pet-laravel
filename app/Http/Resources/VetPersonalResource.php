<?php

namespace App\Http\Resources;

use App\Http\Resources\Vet\VetBankResource;
use App\Models\VetPersonalInformation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetPersonalInformation
 */
class VetPersonalResource extends JsonResource
{
    public static $wrap = 'passport';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'personal_id' => $this->id,
            'address' => $this->address,
            'passport_series' => $this->passport_series,
            'passport_number' => $this->passport_number,

            'vet_work_type_id' => $this->vet_work_type_id,
            'educations' => EducationResource::collection($this->educations),

            'bank' => VetBankResource::make($this->bankDetail),
            'bank_id' => $this->bankDetail?->id,
        ];
    }
}
