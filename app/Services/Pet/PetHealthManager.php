<?php

namespace App\Services\Pet;

use App\Contracts\Pet\IPetHealthManager;
use App\Http\Requests\Api\Pet\ManagePetRequest;
use App\Models\HealthConcern;
use App\Models\Pet;

class PetHealthManager implements IPetHealthManager
{
    private Pet $pet;
    private ManagePetRequest $request;

    public function getHealthFromRequest(ManagePetRequest $request): ?HealthConcern
    {
        $health = $request->input('health_concern');

        if (!empty($health) && is_array($health)) {
            if (!empty($health['id']))
                $healthConcern = HealthConcern::findOrFail($health['id']);
            else
                $healthConcern = new HealthConcern($health);
        }

        return $healthConcern ?? null;
    }

    public function set(Pet $pet, ManagePetRequest $request): IPetHealthManager
    {
        $this->pet = $pet;
        $this->request = $request;
        return $this;
    }

    public function save(): Pet
    {
        $health = $this->getHealthFromRequest($this->request);

        if ($health) {
            $health->pet_id = $this->pet->id;
            $health->saveOrFail();
        }

        return $this->pet;
    }

    public function update(): Pet
    {
        $health = $this->getHealthFromRequest($this->request);

        if ($health) {
            $health->pet_id = $this->pet->id;
            $health->updateOrFail($this->request->input('health_concern'));
        }

        return $this->pet;
    }
}
