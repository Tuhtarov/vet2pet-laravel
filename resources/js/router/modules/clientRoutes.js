import {setLayout, setNavLink} from "@/router/metaUtils";
import Home from "@/views/Home.vue";
import ClientSettings from "@/views/client/ClientSettings.vue"
import petRoutes from "@/router/modules/client/petRoutes";

export default [
    ...petRoutes,

    {
        path: '/vet-services',
        name: 'vet-services',
        component: Home,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Ветеринарные услуги",
                icon: "mdi-medication"
            }),
            role: 'client'
        }
    },
    {
        path: '/consultations',
        name: 'consultations',
        component: Home,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "История консультаций",
                icon: "mdi-format-list-bulleted"
            }),
            role: 'client'
        }
    },

    {
        path: '/settings',
        name: 'settings',
        component: ClientSettings,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Настройки",
                icon: "mdi-cog"
            }),
            role: 'client'
        }
    },
]
