<?php

namespace App\Http\Requests\Api\Consultation;

use Illuminate\Foundation\Http\FormRequest;

class ConsultationResponseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'diagnose' => ['required', 'string', 'max:255'],
            'recommendation' => ['required','string'],
            'vet_consultation_id' => ['required', 'integer'],
            'vet_id' => ['required', 'integer'],
        ];
    }
}
