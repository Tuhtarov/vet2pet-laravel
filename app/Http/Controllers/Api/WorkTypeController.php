<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WorkTypeResource;
use App\Models\VetWorkType;
use Illuminate\Http\Request;

class WorkTypeController extends Controller
{
    public function workTypes()
    {
        return WorkTypeResource::collection(
            VetWorkType::all(),
        );
    }
}
