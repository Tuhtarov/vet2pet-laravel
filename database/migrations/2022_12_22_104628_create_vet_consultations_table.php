<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_consultations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('symptoms');
            $table->text('comment')->nullable();
            $table->date('date_symptoms');
            $table->boolean('is_done')->default(false);

            /* тип консультации */
            $table->foreignId('vet_consultation_type_id')->nullable();
            $table->foreign('vet_consultation_type_id')
                ->on('vet_consultation_types')->references('id');

            /* ветеринарная услуга */
            $table->foreignId('vet_service_id');
            $table->foreign('vet_service_id')
                ->on('vet_services')->references('id');

            /* ветеринар */
            $table->foreignId('vet_id')->nullable();
            $table->foreign('vet_id')
                ->on('users')->references('id');

            /*  питомец */
            $table->foreignId('pet_id');
            $table->foreign('pet_id')
                ->on('pets')->references('id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vet_consultations');
    }
};
