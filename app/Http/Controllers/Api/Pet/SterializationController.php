<?php

namespace App\Http\Controllers\Api\Pet;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\SterializationResource;
use App\Models\Sterialization;
use Illuminate\Http\Request;

class SterializationController extends Controller
{
    public function index()
    {
        return SterializationResource::collection(
            Sterialization::all()
        );
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        return SterializationResource::make(
            Sterialization::create($data)
        );
    }

    public function show(Sterialization $entity)
    {
        return SterializationResource::make($entity);
    }

    public function update(Request $request, Sterialization $entity)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $entity->update($data);

        return SterializationResource::make($entity);
    }
}
