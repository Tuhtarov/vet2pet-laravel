<?php

namespace App\Http\Resources\Vet;

use App\Models\VetConsultationResponse;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetConsultationResponse
 */
class ConsultationResponseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'diagnose' => $this->diagnose,
            'recommendation' => $this->recommendation,
            'vet_id' => $this->vet_id,
            'vet_consultation_id' => $this->vet_consultation_id,
            'status' => (int) $this->status,
            'reason_deviation' => $this->reason_deviation,
        ];
    }
}
