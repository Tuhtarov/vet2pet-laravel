import baseStore from '@/store/modules/pets/baseStore'

export default {
    ...baseStore('/sterialization'),
    namespaced: true,
};
