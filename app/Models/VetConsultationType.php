<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VetConsultationType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];
}
