<?php

namespace App\Http\Requests\Api\Pet;

use Illuminate\Foundation\Http\FormRequest;

class ManagePetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (request()->request->get('user_id') === null) {

            if (request()->routeIs('pet.store')) {
                $this->merge([
                    'user_id' => request()->user()->id,
                ]);
            }
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if (request()->routeIs('pet.store')) {
            $requestRule = 'required';
        } else {
            $requestRule = 'sometimes';
        }

        return [
            'name' => [$requestRule, 'string'],
            'birthday' => 'date|nullable',
            'weight' => 'numeric|nullable',
            'pet_type_id' => [$requestRule, 'integer'],
            'pet_breed_id' => [$requestRule, 'integer'],
            'chip_number' => 'numeric|nullable',
            'detention_condition_id' => 'integer|nullable',
            'place_condition_id' => 'integer|nullable',
            'food_brand_id' => 'integer|nullable',
            'gender_id' => [$requestRule, 'integer'],
            'origin_id' => 'integer|nullable',
            'sterialization_id' => 'integer|nullable',
            'nutrition_id' => 'integer|nullable',
            'user_id' => [$requestRule, 'integer'],

            'procedures.*.procedure_type_id' => 'integer|nullable',
            'procedures.*.reason' => 'string|nullable',
            'procedures.*.procedure_date' => 'date|nullable',
            'procedures.*.pet_id' => 'integer|nullable',
            'procedures.*.id' => 'sometimes|integer',

            'health_concerns' => 'string|nullable',
        ];
    }
}
