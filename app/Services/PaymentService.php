<?php

namespace App\Services;

use YooKassa\Client;

class PaymentService
{
    public function getClient(): Client
    {
        $client = new Client();
        $client->setAuth(config('services.yookasa.shop_id'), config('services.yookasa.secret_key'));

        return $client;
    }

    public function createPayment(float $amount, string $description, array $options = []): string
    {
        $client = $this->getClient();

        $payment = $client->createPayment([
            'amount' => [
                'value' => $amount,
                'currency' => 'RUB'
            ],
            'capture' => true,
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => route('payment.success')
            ],
            'metadata' => [
                'transaction_id' => $options['transaction_id'],
                'package_id' => $options['package_id'],
                'user_id' => $options['user_id']
            ],
            'description' => $description
        ], uniqid('', true));

        return $payment->getConfirmation()->getConfirmationUrl();
    }
}
