<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\IClientCreator;
use App\Contracts\IVetCreator;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function clientValidator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'surname' => ['required', 'string', 'min:2'],
            'patronymic' => ['nullable', 'string', 'min:4'],
            'phone' => ['required', 'integer', 'min:9'],
            'birthday' => ['required', 'date'],
            'city' => ['required', 'string', 'min:2'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function vetValidator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'surname' => ['required', 'string', 'min:2'],
            'patronymic' => ['nullable', 'string', 'min:2'],
            'phone' => ['required', 'integer', 'min:9'],
            'birthday' => ['required', 'date'],
            'city' => ['required', 'string', 'min:2'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8',],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],

            'vet_work_type_id' => ['required', 'integer'],
            'address' => ['required', 'string'],
            'passport_series' => ['required', 'integer'],
            'passport_number' => ['required', 'integer'],
            'education_name' => ['required', 'string'],

            'settlement_account' => ['required', 'integer'],
            'bank_name' => ['required', 'string'],
            'bank_address' => ['required', 'string'],
            'bank_correspondent_account' => ['required', 'integer'],
            'bik' => ['required', 'integer'],
        ]);
    }

    public function clientRegister(Request $request, IClientCreator $creator)
    {
        $user = $creator->create(
            $this->clientValidator($request->all())->validate()
        );
        event(new Registered($user));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }

    public function vetRegister(Request $request, IVetCreator $creator)
    {
        $user = $creator->create(
            $this->vetValidator($request->all())->validate()
        );

        event(new Registered($user));
        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }
}
