<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PetPhoto
 *
 * @property int $id
 * @property int $pet_id
 * @property int $file_id
 * @property-read \App\Models\File|null $image
 * @property-read \App\Models\Pet|null $pet
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto wherePetId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\File $file
 * @property int|null $is_current
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto whereIsCurrent($value)
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Query\Builder|PetPhoto onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PetPhoto whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|PetPhoto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PetPhoto withoutTrashed()
 */
class PetPhoto extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $timestamps = false;

    protected $fillable = [
        'pet_id',
        'file_id',
        'is_current'
    ];

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}
