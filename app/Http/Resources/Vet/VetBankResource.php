<?php

namespace App\Http\Resources\Vet;

use App\Models\VetBankDetail;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetBankDetail
 */
class VetBankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'bank_name' => $this->bank_name,
            'bank_address' => $this->bank_address,
            'bank_correspondent_account' => $this->bank_correspondent_account,
            'settlement_account' => $this->settlement_account,
            'bik' => $this->bik,
            'vet_personal_information_id' => $this->vet_personal_information_id,
            'bank_id' => $this->id,
        ];
    }
}
