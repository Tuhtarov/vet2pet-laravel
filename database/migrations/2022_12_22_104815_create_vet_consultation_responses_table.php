<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_consultation_responses', function (Blueprint $table) {
            $table->id();
            $table->string('diagnose');
            $table->text('recommendation');

            /* ветеринар, оставивший анамнез */
            $table->foreignId('vet_id');
            $table->foreign('vet_id')->on('users')->references('id');

            /* консультация */
            $table->foreignId('vet_consultation_id');
            $table->foreign('vet_consultation_id')
                ->on('vet_consultations')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vet_consultation_responses');
    }
};
