<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_consultation_histories', function (Blueprint $table) {
            $table->id();

            /* консультация */
            $table->foreignId('vet_consultation_id');
            $table->foreign('vet_consultation_id')
                ->on('vet_consultations')->references('id');

            $table->foreignId('vet_consultation_history_type_id');
            $table->foreign('vet_consultation_history_type_id', 'histories_to_type_fk')
                ->on('vet_consultation_history_types')->references('id');

            $table->foreignId('user_id');
            $table->foreign('user_id')
                ->on('users')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vet_consultation_histories', function (Blueprint $table) {
            $table->dropForeign('histories_to_type_fk');
        });
        Schema::dropIfExists('vet_consultation_histories');
    }
};
