import My from "@/views/client/pets/My.vue";
import {setLayout, setNavLink} from "@/router/metaUtils";
import Form from "@/views/client/pets/Form.vue";
import Show from "@/views/client/pets/Show.vue";

export default [
    {
        path: '/my-pets',
        name: 'my-pets',
        component: My,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Мои питомцы",
                icon: "mdi-dog-side"
            }),
            role: 'client'
        }
    },
    {
        path: '/my-pets/add',
        name: 'my-pets-add',
        component: Form,
        meta: {
            ...setLayout('App'),
            role: 'client'
        }
    },
    {
        path: '/my-pets/:id',
        name: 'pet-show',
        component: Show,
        meta: {
            ...setLayout('App'),
            role: 'client'
        }
    },
    {
        path: '/my-pets/edit/:id',
        name: 'pet-edit',
        component: Form,
        meta: {
            ...setLayout('App'),
            role: 'client'
        }
    },
];
