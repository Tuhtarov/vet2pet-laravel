<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientBankDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'settlement_account',
        'expiration',
        'owner',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
