<?php

namespace App\Services;

use App\Contracts\IClientCreator;
use App\Contracts\IUserRegister;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ClientCreator extends IUserRegister implements IClientCreator
{
    public function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            $user = new User($this->register($data, 'client'));
            $user->save();

            return $user;
        });
    }
}
