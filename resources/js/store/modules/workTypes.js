import axios from '@/api/Axios';
export default {
    state: {
        types: [],
    },
    getters: {
        allTypes: state => state.types,
    },
    mutations: {
        setAllWorkTypes: (state, types) => {
            state.types = types
        },
    },
    actions: {
        async fetchAllWorkTypes({commit}) {
            return await axios.get("/vet/workTypes")
                .then(res => {
                    commit('setAllWorkTypes', res.data)
                    return res
                })
        },
    },
    namespaced: true,
}
