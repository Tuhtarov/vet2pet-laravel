<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pets', function (Blueprint $table) {
            $table->foreignId('place_condition_id')->nullable();
            $table->foreign('place_condition_id')
                ->on('place_conditions')->references('id');

            $table->dropColumn('chip_number');
        });

        Schema::table('pets', function (Blueprint $table) {
            $table->string('chip_number', 15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pets', function (Blueprint $table) {
            $table->dropForeign('pets_place_condition_id_foreign');
            $table->dropColumn('place_condition_id');
            $table->dropColumn('chip_number');
        });

        Schema::table('pets', function (Blueprint $table) {
            $table->unsignedInteger('chip_number')->nullable();
        });
    }
};
