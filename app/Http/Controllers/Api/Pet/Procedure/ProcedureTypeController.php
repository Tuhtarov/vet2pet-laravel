<?php

namespace App\Http\Controllers\Api\Pet\Procedure;

use App\Http\Controllers\Controller;
use App\Http\Resources\Pet\ProcedureTypeResource;
use App\Models\PetProcedureType;
use Illuminate\Http\Request;

class ProcedureTypeController extends Controller
{
    public function index()
    {
        return ProcedureTypeResource::collection(
            PetProcedureType::all()
        );
    }
}

