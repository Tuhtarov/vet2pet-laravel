<?php

namespace App\Http\Controllers\Api\Pet;

use App\Contracts\IFileUploader;
use App\Contracts\Pet\IPetProcedureManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Pet\ManagePetRequest;
use App\Http\Resources\Pet\PetResource;
use App\Models\Pet;
use App\Models\PetPhoto;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PetController extends Controller
{
    public function __construct(
        private readonly IFileUploader $uploader,
        private readonly IPetProcedureManager $procedureService
    )
    {
    }

    public function index()
    {
        return PetResource::collection(
            Pet::all()
        );
    }

    public function show(Pet $pet)
    {
        return PetResource::make($pet);
    }

    public function store(ManagePetRequest $request)
    {
        $pet = new Pet($request->validated());

        if ($pet->save()) {
            $this->savePhotos($request, $pet);
            $this->procedureService->saveProceduresFor($pet, $this->procedureService->getProceduresFrom($request));
        }

        return PetResource::make(
            Pet::find($pet->id)
        );
    }

    public function update(ManagePetRequest $request, Pet $pet)
    {
        $data = $request->validated();

        if ($pet->update($data)) {
            $this->savePhotos($request, $pet);
            $this->procedureService->updateProceduresFor($pet, $request);
        }

        return PetResource::make(
            Pet::find($pet->id)
        );
    }

    public function savePhotos(Request $request, Pet $pet)
    {
        $result = false;

        if (!empty($request->allFiles())) {
            foreach ($request->files as $file) {
                $petPhoto = new PetPhoto();
                $petPhoto->pet_id = $pet->id;
                $petPhoto->file_id = $this->uploader->handle($file)->id;
                $result = $petPhoto->save();
            }

            if (isset($petPhoto)) {
                foreach (PetPhoto::wherePetId($pet->id)->get()->all() as $existingPhoto) {
                    $existingPhoto->is_current = false;
                    $existingPhoto->save();
                }

                $petPhoto->is_current = true;
                $petPhoto->save();
            }
        }

        return $result;
    }

    public function setCurrentPhoto(Request $request, Pet $pet)
    {
        $result = false;

        if (!empty($request->input('pet_photo_id'))) {
            $petPhoto = PetPhoto::whereId($request->input('pet_photo_id'))->firstOrFail();
            $petPhoto->is_current = true;

            if ($petPhoto->pet_id == $pet->id) {
                foreach (PetPhoto::wherePetId($pet->id)->get()->all() as $existingPhoto) {
                    $existingPhoto->is_current = false;
                    $existingPhoto->save();
                }

                $result = $petPhoto->save();
            }
        }

        return response()->json(['result' => $result]);
    }

    public function myPets(Request $request)
    {
        return PetResource::collection(
            Pet::where('user_id', $request->user()->id)->get()
        );
    }

    public function deletePhoto(PetPhoto $photo)
    {
        return response()->json([
            'result' => $photo->delete()
        ]);
    }

    public function destroy(Pet $pet)
    {
        $currentUser = request()->user();

        if ($currentUser->roleSlug() === 'admin') {
            return $this->deletePet($pet);
        }

        if ($currentUser->id == $pet->user_id) {
            return $this->deletePet($pet);
        }

        throw new \InvalidArgumentException("Вам нельзя удалять чужих питомцев.");
    }

    private function deletePet(Pet $pet): JsonResponse
    {
        return response()->json([
            'status' => $pet->delete() ? 'Pet deleted successfully' : 'not deleted'
        ]);
    }
}
