import {setLayout, setNavLink} from "@/router/metaUtils";
import VetServices from "@/views/admin/vets/VetServices.vue";
import ServiceAdd from "@/views/admin/vets/ServiceAdd.vue";

export default [
    {
        path: '/services',
        name: 'services',
        component: VetServices,
        meta: {
            ...setLayout('App'),
            ...setNavLink({
                title: "Ветеринарные услуги",
                icon: "mdi-medication"
            }),
            role: 'admin'
        }
    },

    {
        path: '/services-add',
        name: 'services-add',
        component: ServiceAdd,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },

    {
        path: '/service-edit/:id',
        name: 'service-edit',
        component: ServiceAdd,
        meta: {
            ...setLayout('App'),
            role: 'admin'
        }
    },
]
