<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PetProcedure
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $procedure_date
 * @property string|null $reason
 * @property int|null $pet_id
 * @property int $procedure_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pet
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PetProcedureType[] $procedureTypes
 * @property-read int|null $procedure_types_count
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure query()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure wherePetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereProcedureDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereProcedureTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\PetProcedureType $procedureType
 * @method static \Illuminate\Database\Query\Builder|PetProcedure onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PetProcedure whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|PetProcedure withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PetProcedure withoutTrashed()
 */
class PetProcedure extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'procedure_date',
        'reason',
        'pet_id',
        'procedure_type_id',
    ];

    protected $casts = [
        'procedure_date' => 'date',
    ];

    public function pet()
    {
        return $this->hasOne(Pet::class);
    }

    public function procedureType()
    {
        return $this->belongsTo(PetProcedureType::class, 'procedure_type_id', 'id');
    }
}
