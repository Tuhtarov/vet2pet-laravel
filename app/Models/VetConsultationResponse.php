<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VetConsultationResponse
 *
 * @property int $id
 * @property string $diagnose
 * @property string $recommendation
 * @property int $vet_id
 * @property int $vet_consultation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereDiagnose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereRecommendation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereVetConsultationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetConsultationResponse whereVetId($value)
 * @mixin \Eloquent
 */
class VetConsultationResponse extends Model
{
    use HasFactory;

    protected $fillable = [
        'vet_consultation_id',
        'vet_id',
        'diagnose',
        'recommendation',
    ];

    public function vet()
    {
        return $this->belongsTo(User::class, 'vet_id', 'id');
    }

    public function vetConsultation()
    {
        return $this->belongsTo(VetConsultation::class, 'vet_consultation_id', 'id');
    }
}
