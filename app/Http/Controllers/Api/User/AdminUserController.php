<?php

namespace App\Http\Controllers\Api\User;

use App\Contracts\IClientCreator;
use App\Contracts\IVetCreator;
use App\Contracts\IVetUpdater;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\AdminManageRequest;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function index(Request $request)
    {
        $authUser = $request->user();
        $isAdmin = $authUser->role->slug === 'admin';

        if (!$isAdmin) {
            throw new Exception('Вы не администратор', 400);
        }

        return UserResource::collection(User::all());
    }

    public function store(AdminManageRequest $request, IClientCreator $clientCreator, IVetCreator $vetCreator)
    {
        $data = $request->validated();
        $role = Role::findOrFail($data['role_id']);

        if ($role->slug === 'vet') {
            $user = $vetCreator->create($data);
        } else {
            $user = $clientCreator->create($data);
        }

        return UserResource::make(User::find($user->id));
    }

    public function show(Request $request, User $user)
    {
        if ($request->user()->role->slug === 'admin') {
            return UserResource::make($user);
        }

        throw new Exception('Вы не администратор', 400);
    }

    public function update(AdminManageRequest $request, User $user, IVetUpdater $vetUpdater)
    {
        $data = $request->validated();
        $role = Role::findOrFail($data['role_id']);

        if ($role->slug === 'vet') {
            $user = $vetUpdater->update($user, $data);
        } else {
            $user->update($data);
        }

        return UserResource::make(
            User::find($user->id)
        );
    }
}
