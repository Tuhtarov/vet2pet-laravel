import {axiosLogin} from '@/api/Axios';
import axios from '@/api/Axios';

export default {
    state: {
        user: null,
        vets: null,
        roles: [],
        users: []
    },
    getters: {
        user: state => state.user,
        roles: state => state.roles,
        allUsers: state => state.users,
        allVets: state => state.vets,
        loading: state => state.user === null,
    },
    mutations: {
        setUser: (state, val) => {
            state.user = val
        },

        setRoles: (state, val) => {
            state.roles = val
        },

        setUsers: (state, val) => {
            state.users = val
        },

        setVets: (state, val) => {
            state.vets = val
        },
    },
    actions: {
        async fetchCurrentUser({commit, getters}) {
            if (getters.user !== null) {
                return getters.user;
            }

            return await axios.get('/user').then(({data: {user}}) => {
                commit('setUser', user);
                return user;
            });
        },

        async fetchAllVets({commit}) {
            return await axios.get("/user/vets").then(({data}) => {
                commit('setVets', data)
                return data
            })
        },

        async fetch(_, id) {
            return await axios.get(`/user/${id}`).then(({data: {user}}) => {
                return user
            })
        },

        async fetchAllUsers({commit, getters}) {
            return await axios.get('/user/all').then(({data}) => {
                commit('setUsers', data);
                return data;
            });
        },

        async fetchRoles({commit}) {
            return await axios.get('/role').then(({data}) => {
                commit('setRoles', data);
                return data;
            });
        },

        async reloadUser({commit, getters}) {
            return await axios.get('/user').then(({data: {user}}) => {
                commit('setUser', user);
                return getters.user;
            });
        },

        async updateMyPassword({commit}, passwordFields) {
            return await axios.post('/user/password-update', passwordFields)
        },

        async updateMyContact({commit}, contactFields) {
            return await axios.post('/user/update', contactFields)
        },

        async signIn({dispatch}, user) {
            return dispatch('auth', {user, url: '/login'})
        },

        async clientRegister({dispatch}, user) {
            return dispatch('auth', {user, url: '/client/register'})
        },

        async vetRegister({dispatch}, user) {
            return dispatch('auth', {user, url: '/vet/register'})
        },

        /** создание пользователей админом */
        async adminCreate(_, user) {
            return await axios.post(`/user/admin-store`, user)
                .then(r => r.data.user)
        },

        /** обновление пользователей админом */
        async adminUpdate(_, user) {
            return await axios.post(`/user/admin-update/${user.id}`, user)
                .then(r => r.data.user)
        },

        async auth({dispatch}, data) {
            const url = data.url
            const user = data.user
            const axiosInstance = url === '/login' ? axiosLogin : axios

            const response = await axios.get('/sanctum/csrf-cookie')
                .then(() => axiosInstance.post(url, user))

            const authenticated = response.status === 204 || response.status === 200 || response.status === 201

            if (authenticated) {
                await dispatch('fetchCurrentUser')
            } else {
                return response
            }

            return authenticated;
        },


        async logout({commit}) {
            return await axiosLogin.post('/logout')
        },

        dropUser({commit}) {
            commit('setUser', null)
        }
    },
    namespaced: true,
}
