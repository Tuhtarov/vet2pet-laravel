import {createStore} from 'vuex'
import users from "@/store/modules/user.js";
import workTypes from "@/store/modules/workTypes";
import personal from "@/store/modules/personal";
import vets from "@/store/modules/vets";
import files from "@/store/modules/files";
import educations from "@/store/modules/educations";
import petTypes from "@/store/modules/pets/petTypes";
import petBreeds from "@/store/modules/pets/petBreeds";
import petDetentionConditions from "@/store/modules/pets/petDetentionConditions";
import petFoods from "@/store/modules/pets/petFoods";
import petNutrition from "@/store/modules/pets/petNutrition";
import petOrigins from "@/store/modules/pets/petOrigins";
import pets from "@/store/modules/pets";
import vetServices from "@/store/modules/vetServices";
import petPlaceConditions from "@/store/modules/pets/petPlaceConditions";
import genders from "@/store/modules/genders";
import sterializations from "@/store/modules/pets/sterializations";
import consultationTypes from "@/store/modules/consultationTypes";
import consultations from "@/store/modules/vets/consultations";
import consultationResponses from "@/store/modules/vets/consultations-response";
import chat from "@/store/modules/chat";


const theme = localStorage.getItem('theme') ?? 'light'

export default createStore({
    state: {
        theme: theme
    },
    getters: {
        theme: ({theme}) => theme,
        themeIcon: ({theme}) => theme === 'light' ? 'mdi-weather-sunny' : 'mdi-weather-night',
    },
    mutations: {
        setTheme(state, val) {
            state.theme = val;
            localStorage.setItem('theme', val)
        }
    },
    actions: {
        async switchTheme({getters, commit}) {
            return await commit('setTheme', getters.theme === 'light' ? 'dark' : 'light')
        },
    },
    modules: {
        users,
        workTypes,
        personal,

        vets,
        vetServices,

        consultationTypes,
        consultations,
        consultationResponses,

        files,
        educations,

        pets,
        petTypes,
        petBreeds,
        petDetentionConditions,
        petPlaceConditions,
        petFoods,
        petNutrition,
        petOrigins,
        genders,
        chat,
        sterializations
    },
})
