<?php

namespace App\Http\Requests\Api\Client\Consultation;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "symptoms" => ['required', 'string', 'max:255', 'min:4'],
            "date_symptoms" => ['date'],
            "comment" => ['string', 'nullable'],
            "pet_id" => ['nullable', 'integer'],
            "vet_consultation_type_id" => ['nullable', 'integer'],
            "vet_service_id" => ['required', 'integer'],
            "client_id" => ['nullable', 'integer'],
        ];
    }
}
