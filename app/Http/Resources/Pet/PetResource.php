<?php

namespace App\Http\Resources\Pet;

use App\Http\Resources\UserResource;
use App\Models\Pet;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Pet
 */
class PetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'name' => $this->name,
            'weight' => (float)$this->weight,
            'chip_number' => $this->chip_number,

            'pet_type_id' => $this->pet_type_id,
            'pet_type' => $this->petType?->name,

            'detention_condition_id' => $this->detention_condition_id,
            'detention' => $this->detention?->name,

            'place_condition_id' => $this->place_condition_id,
            'place' => $this->place?->name,

            'image' => PetPhotoResource::make($this->image),
            'image_path' => $this->image?->file?->path,

            'photos' => PetPhotoResource::collection($this->photos),

            'food_brand_id' => $this->food_brand_id,
            'food_brand' => $this->foodBrand?->name,

            'pet_breed_id' => $this->pet_breed_id,
            'pet_breed' => $this->breed?->name,

            'gender_id' => $this->gender_id,
            'gender' => $this->gender?->name,

            'origin_id' => $this->origin_id,
            'origin' => $this->origin?->name,

            'sterialization_id' => $this->sterialization_id,
            'sterialization' => $this->sterialization?->name,

            'nutrition_id' => $this->nutrition_id,
            'nutrition' => $this->nutrition?->name,

            'user_id' => $this->user_id,
            'user' => UserResource::make($this->user),

            'created_at' => $this->created_at,
            'created' => $this->created_at?->format('Y-m-d H:i:s'),

            'birthday' => $this->birthday,

            'procedures' => ProcedureResource::collection($this->procedures),

            'health_concerns' => $this->health_concerns,
        ];
    }
}
