import {axiosForUploading} from '@/api/Axios';

const createFilesForm = files => {
    let form = new FormData();

    for (let file in files) {
        form.append(`file${file}`, files[file]);
    }

    return form
}

export default {
    state: {},
    getters: {},
    mutations: {},
    actions: {
        async addEducationImages(store, fields) {
            return await axiosForUploading
                .post(`/education/${fields.id}/add-images`, createFilesForm(fields.files))
        },
    },
    namespaced: true,
}
