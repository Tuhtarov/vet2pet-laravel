<?php

namespace App\Services;

use App\Contracts\IFileUploader;
use App\Models\File;
use App\Models\FileType;
use Illuminate\Http\UploadedFile;

class FileUploader implements IFileUploader
{
    public function handle(UploadedFile|\Symfony\Component\HttpFoundation\File\UploadedFile $file): File
    {
        $fileBasePath = '/files/' . date('Y-m');
        $fileStorePath = public_path() . '/files/' . date('Y-m');

        $ext = $file->getClientOriginalExtension();
        $clientFileName = $file->getClientOriginalName();

        $fileType = FileType::whereName($ext)->first();

        if (!$fileType) {
            throw new \Exception("В базе данных не существует такого типа файла: $ext");
        }

        // уникальное имя для файла
        $uniqueFileName = bin2hex(random_bytes(10)) . ".$ext";
        $file->move($fileStorePath, $uniqueFileName);

        return File::create([
            'name' => $clientFileName,
            'path' => "$fileBasePath/$uniqueFileName",
            'file_type_id' => $fileType->id,
        ]);
    }
}
