<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\VetEducationFile
 *
 * @property int $id
 * @property int $vet_education_id
 * @property int $file_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\VetEducation|null $education
 * @property-read \App\Models\File|null $file
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereVetEducationId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Query\Builder|VetEducationFile onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|VetEducationFile whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|VetEducationFile withTrashed()
 * @method static \Illuminate\Database\Query\Builder|VetEducationFile withoutTrashed()
 */
class VetEducationFile extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'file_id',
        'vet_education_id',
    ];

    public function education()
    {
        return $this->belongsTo(VetEducation::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
