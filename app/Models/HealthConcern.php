<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HealthConcern
 *
 * @property int $id
 * @property string $name
 * @property int $pet_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Pet|null $pets
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern query()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern wherePetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|HealthConcern whereDescription($value)
 */
class HealthConcern extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'pet_id',
    ];

    public function pets()
    {
        return $this->belongsTo(Pet::class);
    }
}
