<?php

namespace App\Http\Resources;

use App\Models\VetEducation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetEducation
 */
class EducationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->created_at?->format('m.d.Y H:i'),

            'files' => EducationFilesResource::collection($this->files)
        ];
    }
}
