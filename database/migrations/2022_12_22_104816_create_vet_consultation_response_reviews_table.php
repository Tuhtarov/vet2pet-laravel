<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vet_consultation_response_reviews', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('stars')->default(5);
            $table->text('comment');

            /* клиент, оставляющий отзыв */
            $table->foreignId('client_id');
            $table->foreign('client_id')->on('users')->references('id');

            /* анамнез консультации */
            $table->foreignId('vet_consultation_response_id');
            $table->foreign('vet_consultation_response_id', 'reviews_on_consultation_fk')
                ->on('vet_consultation_responses')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vet_consultation_response_reviews', function (Blueprint $table) {
            $table->dropForeign('reviews_on_consultation_fk');
        });
        Schema::dropIfExists('vet_consultation_response_reviews');
    }
};
