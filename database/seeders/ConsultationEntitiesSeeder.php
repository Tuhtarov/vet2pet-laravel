<?php

namespace Database\Seeders;

use App\Models\VetConsultationType;
use Illuminate\Database\Seeder;

class ConsultationEntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Экстренная консультация', 'Плановая консультация'] as $val) {
            $entity = new VetConsultationType();
            $entity->name = $val;
            $entity->save();
        }
    }
}
