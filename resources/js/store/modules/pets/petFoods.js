import baseStore from '@/store/modules/pets/baseStore'

export default {
    ...baseStore('/food-brand'),
    namespaced: true,
};
