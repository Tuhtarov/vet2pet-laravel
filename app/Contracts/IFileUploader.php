<?php

namespace App\Contracts;

use App\Models\File;
use Illuminate\Http\UploadedFile;

interface IFileUploader
{
    public function handle(UploadedFile $file): File;
}
