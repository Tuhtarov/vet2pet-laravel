<?php

namespace App\Http\Resources\Pet;

use App\Models\Nutrition;
use App\Models\PetProcedure;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PetProcedure
 */
class ProcedureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reason' => $this->reason,
            'procedure_date' => $this->procedure_date?->format('Y-m-d'),
            'procedure_type_id' => $this->procedure_type_id,
            'procedure_type_name' => $this->procedureType?->name,
            'pet_id' => $this->pet_id,
            'created_at' => $this->created_at,
        ];
    }
}
