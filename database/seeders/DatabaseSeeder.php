<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        (new PetEntitiesSeeder())->run();
        (new VetEntitiesSeeder())->run();
        (new RoleEntitiesSeeder())->run();
        (new FileTypesSeeder())->run();
        (new ConsultationEntitiesSeeder())->run();
    }
}
