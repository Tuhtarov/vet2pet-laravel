<?php

namespace App\Http\Resources\Client\Consultation;

use App\Http\Resources\Pet\PetResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Vet\ConsultationResponseResource;
use App\Http\Resources\VetServiceResource;
use App\Models\VetConsultation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VetConsultation
 */
class ConsultationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'symptoms' => $this->symptoms,
            'date_symptoms' => $this->date_symptoms,
            'comment' => $this->comment,
            'is_done' => $this->is_done,

            'vet_response' => ConsultationResponseResource::make($this->response),

            'vet_id' => $this->vet_id,
            'vet_full_name' => $this->vet?->getFullName(),
            'vet' => UserResource::make($this->vet),

            'pet_id' => $this->pet_id,
            'pet_name' => $this->pet?->name,

            'vet_service_id' => $this->vet_service_id,
            'service_price' => $this->service->price,
            'service' => VetServiceResource::make($this->service),

            'vet_consultation_type_id' => $this->vet_consultation_type_id,
            'vet_consultation' => $this->consultationType,

            'attachments' => AttachmentsResource::collection($this->attachments),

            'created' => $this->created_at?->format('Y-m-d H:i')
        ];
    }
}
