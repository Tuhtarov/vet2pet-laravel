<?php

namespace Database\Seeders;

use App\Models\FileType;
use App\Models\Role;
use Illuminate\Database\Seeder;

class FileTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['png', 'jpeg', 'jpg'] as $val) {
            $entity = new FileType();
            $entity->name = $val;
            $entity->save();
        }
    }
}
