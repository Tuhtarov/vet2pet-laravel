<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    public function update(Request $request, User $user = null)
    {
        $request->validate([
            'city' => ['required', 'string'],
            'address' => ['required', 'string'],
            'passport_series' => ['required', 'integer'],
            'passport_number' => ['required', 'integer'],
        ]);

        /** @var User $authUser */
        $authUser = $request->user();
        $isAdmin = $authUser->role->slug === 'admin';

        return response()->json(
            $this->updatePassport(
                !empty($user) && $isAdmin ? $user : $authUser, $request
            )
        );
    }

    private function updatePassport(User $user, Request $request): bool
    {
        $user->city = $request->get('city');
        return $user->save() && $user->vetPersonal?->update($request->all());
    }
}
