import axios from '@/api/Axios';

export default {
    state: {
        response: [],
    },

    getters: {
        all: state => state.response,
    },

    mutations: {
        add: (state, data) => {
            state.response.push(data);
        },
    },

    actions: {
        async create({commit}, params) {
            await axios.post(`/consultation-response`, params)
                .then(({data}) => {
                    commit('add', data);
                    return data;
                })
        },
        async update({commit}, params) {
            await axios.put(`/consultation-response/${params.get('id')}`, params)
                .then(({data}) => {
                    return data;
                })
        },
        async fetch({commit}, id) {
            return await axios.get(`/consultation-response/${id}`).then(res => {
                return res.data
            })
        },
        async changeStatus({commit}, params) {
            await axios.patch(`/consultation-response/${params.get('id')}/change/status`, params)
                .then(({data}) => {
                    return data;
                })
        },
    },

    namespaced: true,
}
